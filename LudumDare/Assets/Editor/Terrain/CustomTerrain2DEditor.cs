﻿using UnityEngine;	
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

// Bad uvs
// Cancel doesn't work well

[CustomEditor(typeof(CustomTerrain2D))]
public class CustomTerrain2DEditor : Editor
{
	CustomTerrain2D terrain;

	string handlePath = "CustomTerrain2D/PointHandle/";
	bool isLooping = false;
    
	void OnEnable()
	{
		terrain = target as CustomTerrain2D;
		isLooping = terrain.loop;
	}

	public void Clear()
	{
		Undo.SetCurrentGroupName("Remove terrain sections");
		ClearSection();
		Undo.RecordObject(terrain, "Terrain point cleared");
		terrain.points.Clear();
	}

	public void ClearSection()
	{
		terrain.GeneratedMesh = new Mesh();
		terrain.section.pCollider.points = new Vector2[0];
	}

	class MeshAttribsGenerationContainer
	{
		public MeshAttribsGenerationContainer(int verticesCount, int uvsCount, int indicesCount)
		{
			vertices = new Vector3[verticesCount];
			uvs = new Vector2[uvsCount];
			indices = new int[indicesCount];
		}

		public void AssignCurrentVertice(Vector3 value)
		{
			vertices[verticesIndex++] = value;
		}
		public void AssignCurrentUV(Vector2 value)
		{
			uvs[uvIndex++] = value;
		}
		public void AssignCurrentIndice(int value)
		{
			indices[indiceIndex++] = value;
		}

		public Vector3[] vertices;
		public int verticesIndex = 0;

		public Vector2[] uvs;
		public int uvIndex = 0;

		public int[] indices;
		public int indiceIndex = 0;
	}

	public void OnEditorChanged()
	{
		int terrainPointCount = terrain.points.Count;
		if (terrainPointCount <= 1)
			return;

		Vector3 leftAxis;
		if (terrain.loop)
			leftAxis = terrain.points[terrainPointCount - 1] - terrain.points[0];
		else
			leftAxis = terrain.points[0] - terrain.points[1];

		Vector3 rightAxis = Vector3.up;

		int pointsCount = (terrainPointCount + (terrain.loop ? 1 : 0)) * 2;

		Vector3[] points = new Vector3[pointsCount];
		int[] uvMultiplier = new int[terrainPointCount + (terrain.loop ? 1 : 0)]; 

		for (int i = 0; i < terrainPointCount; ++i)
		{
			if (i == terrainPointCount - 1)
			{
				if (terrain.loop)
					rightAxis = terrain.points[0] - terrain.points[i];
				else
					rightAxis = -leftAxis;
			}
			else
			{
				uvMultiplier[i] = ComputeTiling(terrain.preferedTileLength, terrain.points[i], terrain.points[i + 1]);
				rightAxis = terrain.points[i + 1] - terrain.points[i];
			}

			Vector3 axis = ComputePointAxis(leftAxis, rightAxis);
			points[i * 2] = terrain.points[i] + axis;
			points[i * 2 + 1] = terrain.points[i] - axis;
			
			leftAxis = -rightAxis;

		}
		if (terrain.loop)
		{
			points[terrainPointCount * 2] = points[0];
			points[terrainPointCount * 2 + 1] = points[1];

			uvMultiplier[terrainPointCount] = ComputeTiling(terrain.preferedTileLength, terrain.points[terrainPointCount - 1], terrain.points[0]);
		}

		FillCollider(terrain.section.pCollider, points);
		FillMeshAttribs(terrain, points, uvMultiplier);
	}

	public int ComputeTiling(float preferedTileLength, Vector3 a, Vector3 b)
	{
		float totalDistance = Vector3.Distance(a, b);
		int res = (int)(totalDistance / preferedTileLength);
		return res > 1 ? res : 1;
	}

	public Vector3 ComputePointAxis(Vector3 leftAxis, Vector3 rightAxis)
	{
		float halfVerticalSize = terrain.verticalSize / 2f;

		float angle = Vector3.Angle(rightAxis, leftAxis) / 2f * Mathf.Sign(Vector3.Cross(rightAxis, leftAxis).z);
		Vector3 leftMiddleVector = Quaternion.AngleAxis(angle, Vector3.forward) * rightAxis.normalized;

		if (angle != 0)
		{
			Vector3 temp = leftMiddleVector * halfVerticalSize / Mathf.Sin(angle * Mathf.Deg2Rad);
			return temp;
		}
		return Vector3.up;
	}

	void FillCollider(PolygonCollider2D collider, Vector3[] points)
	{
		Vector2[] colliderPoints = new Vector2[points.Length];

		int dir = 1;
		int pointIndex = 0;
		int colliderPoint = 0;
		while (pointIndex >= 0)
		{
			colliderPoints[colliderPoint] = points[pointIndex];

			pointIndex += 2 * dir;
			colliderPoint++;
			// restart for odd points
			if (pointIndex >= points.Length)
			{
				pointIndex = points.Length - 1;
				dir = -1;
			}
		}

		collider.points = colliderPoints;
	}

	/// <summary>
	/// Produce a mesh and set it to the terrain.
	/// </summary>
	/// <param name="terrainToFill">The terrain that contain the mesh to produce.</param>
	/// <param name="points">All the points of the mesh</param>
	/// <param name="uvMultiplier">The tiling amount of the texture for each rect. It will produce uvMultiplier tiles for all the corresponding rect.</param>
	void FillMeshAttribs(CustomTerrain2D terrainToFill, Vector3[] points, int[] uvMultiplier)
	{
		int pointCount = points.Length;
		MeshAttribsGenerationContainer container = new MeshAttribsGenerationContainer((pointCount - 2) * 2, (pointCount - 2) * 2, (pointCount - 2) * 3);

		for (int i = 0; i < pointCount - 2; i += 2)
		{
			container.AssignCurrentIndice(container.verticesIndex + 0);
			container.AssignCurrentIndice(container.verticesIndex + 2);
			container.AssignCurrentIndice(container.verticesIndex + 1);
			container.AssignCurrentIndice(container.verticesIndex + 1);
			container.AssignCurrentIndice(container.verticesIndex + 2);
			container.AssignCurrentIndice(container.verticesIndex + 3);

			container.AssignCurrentVertice(points[i]);
			container.AssignCurrentVertice(points[i + 1]);
			container.AssignCurrentVertice(points[i + 2]);
			container.AssignCurrentVertice(points[i + 3]);

			container.AssignCurrentUV(new Vector2(0, 1));
			container.AssignCurrentUV(new Vector2(0, 0));
			container.AssignCurrentUV(new Vector2(uvMultiplier[i / 2], 1));
			container.AssignCurrentUV(new Vector2(uvMultiplier[i / 2], 0));
		}

		Mesh mesh = new Mesh();	
		mesh.name = "GeneratedTerrainMesh";
		mesh.vertices = container.vertices;
		mesh.uv = container.uvs;
		mesh.triangles = container.indices;
		mesh.RecalculateBounds();
		mesh.RecalculateNormals();
		terrainToFill.GeneratedMesh = mesh;
	}

	Mesh CreateDefaultMesh()
	{
		Mesh mesh = new Mesh();

		Vector3[] vertices = new Vector3[4];

		Vector2[] uvs = new Vector2[4];
		uvs[0].Set(0, 1);
		uvs[1].Set(0, 0);
		uvs[2].Set(1, 0);
		uvs[3].Set(1, 1);

		int[] triangles = { 0, 1, 2, 0, 2, 3 };

		mesh.vertices = vertices;
		mesh.triangles = triangles;
		mesh.uv = uvs;

		return mesh;
	}

	void OnSceneGUI()
	{
		Handles.matrix = terrain.transform.localToWorldMatrix;
		Tools.current = Tool.None;
		bool needUpdate = false;

		Event e = Event.current;

		needUpdate |= DrawHandles(e);
		if (e.type != EventType.ValidateCommand && e.commandName == "UndoRedoPerformed")
			needUpdate = true;

		if (e.type == EventType.KeyDown && e.keyCode == KeyCode.Delete)
			needUpdate |= DeletePoint(e);

		if (e.shift)
			needUpdate |= AddPointMode(e);

		if (needUpdate)
		{
			EditorUtility.SetDirty(target);
			OnEditorChanged();
		}
	}

	public bool DeletePoint(Event e)
	{
		e.Use();
		Undo.RecordObject(terrain, "Terrain point removed");
		string focusedControlName = GUI.GetNameOfFocusedControl();
		int separatorIndex = focusedControlName.LastIndexOf('/');
		if (separatorIndex == -1)
			return false;
		string path = focusedControlName.Substring(0, separatorIndex + 1);
		if (path == handlePath)
		{
			string handleName = focusedControlName.Substring(separatorIndex + 1);
			int pointIndex;
			if (int.TryParse(handleName, out pointIndex))
			{
				if (pointIndex < 0 || pointIndex >= terrain.points.Count)
					return false;

				terrain.points.RemoveAt(pointIndex);
				return true;
			}
		}
		return false;
	}

    public bool AddPointMode(Event e)
    {
        bool needUpdate = false;

        if (e.type == EventType.KeyDown && e.keyCode == KeyCode.Tab)
        {
            terrain.insert = !terrain.insert;
            e.Use();
        }

        HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));
        /*Vector3 newPoint = new Vector3(e.mousePosition.x, e.mousePosition.y);
		newPoint = Camera.current.ScreenToWorldPoint(newPoint);
		newPoint.z = terrain.transform.position.z;
		newPoint.y = -newPoint.y + 2 * Camera.current.transform.position.y;*/

        var ray = HandleUtility.GUIPointToWorldRay(e.mousePosition);

        Plane p = new Plane(Camera.current.transform.forward, terrain.transform.position);
        float enter;
        Vector3 newPoint;
        if (p.Raycast(ray, out enter))
            newPoint = enter * ray.direction + ray.origin;
        else
            newPoint = ray.origin + ray.direction;

        newPoint = terrain.transform.InverseTransformPoint(newPoint);

        List<Vector3> possiblyLinkedPointsList = null;
        List<int> pointsListIndexMapping = null;
        if (terrain.insert)
            possiblyLinkedPointsList = terrain.points;
        else
        {
            if (terrain.points.Count == 1)
                possiblyLinkedPointsList = new List<Vector3> { terrain.points[0] };
            else if (terrain.points.Count > 1)
            {
                possiblyLinkedPointsList = new List<Vector3> { terrain.points[0], terrain.points[terrain.points.Count - 1] };
                pointsListIndexMapping = new List<int> { 0, terrain.points.Count - 1 };
            }
        }

        int closestPointIndex = GetClosestPointIndex(possiblyLinkedPointsList, newPoint);
        if (pointsListIndexMapping != null)
            closestPointIndex = pointsListIndexMapping[closestPointIndex];

        Vector3 referencePoint = terrain.transform.position;
        if (closestPointIndex >= 0 && closestPointIndex < terrain.points.Count)
            referencePoint = terrain.points[closestPointIndex];

        if (e.control)
        {
            newPoint = terrain.transform.TransformPoint(newPoint);
            newPoint.x = Handles.SnapValue(newPoint.x, terrain.snap.x);
            newPoint.y = Handles.SnapValue(newPoint.y, terrain.snap.y);
            newPoint = terrain.transform.InverseTransformPoint(newPoint);
        }

        Handles.DrawSolidDisc(newPoint, Vector3.forward, terrain.diskSize / 2f);

		int pointCount = terrain.points.Count;
		int linkedNeighbourIndex = -1;

		if (pointCount >= 1)
		{
            if (terrain.insert && pointCount > 1)
                linkedNeighbourIndex = GetLinkedNeighboorIndex(terrain.points, closestPointIndex, newPoint, terrain.loop);

            Handles.DrawDottedLine(terrain.points[closestPointIndex], newPoint, 0.5f);
			if (linkedNeighbourIndex != -1)
				Handles.DrawDottedLine(terrain.points[linkedNeighbourIndex], newPoint, 0.5f);
		}
		if (e.type == EventType.MouseDown && e.button == 0)
		{
			Undo.RecordObject(terrain, "Terrain point created");

			if (linkedNeighbourIndex == -1)
			{
				if (closestPointIndex == terrain.points.Count - 1)
					terrain.points.Add(newPoint);
				else
					terrain.points.Insert(0, newPoint);
			}
			else
			{
				// Loop case
				if (Mathf.Abs(linkedNeighbourIndex - closestPointIndex) != 1)
					terrain.points.Insert(0, newPoint);
				else
					terrain.points.Insert(linkedNeighbourIndex > closestPointIndex ? linkedNeighbourIndex : closestPointIndex, newPoint);
			}

			Undo.IncrementCurrentGroup();
			needUpdate = true;

			e.Use();
		}

		SceneView.RepaintAll();

		return needUpdate;
	}

	int GetClosestPointIndex(List<Vector3> list, Vector3 referencePoint)
	{
        if (list == null || list.Count == 0)
			return -1;

		float closestDistance = Vector3.Distance(list[0], referencePoint);
		int closestIndex = 0;

		for (int i = 1; i < list.Count; ++i)
		{
			float distance = Vector3.Distance(list[i], referencePoint);
			if (distance < closestDistance)
			{
				closestDistance = distance;
				closestIndex = i;
			}
		}

		return closestIndex;
	}

	int GetLinkedNeighboorIndex(List<Vector3> list, int currentPointIndex, Vector3 referencePoint, bool isLooping)
	{
		int listCount = list.Count;
		if (list.Count <= 1)
			return -1;

		// if ternary if true perform modulo, else, modulo always > value so modulo made useless
		int modulo = (isLooping ? listCount : (listCount + 1));
		int infNeighbour = Mod((currentPointIndex - 1), modulo);
		int supNeighbour = Mod((currentPointIndex + 1), modulo);

		float infNeighbourRefPointAngle = 90;
		float supNeighbourRefPointAngle = 90;

		Vector3 currentPoint = list[currentPointIndex];

		if (infNeighbour < listCount)
			infNeighbourRefPointAngle = Vector3.Angle(list[infNeighbour] - currentPoint, referencePoint - currentPoint);
		else
			infNeighbour = -1;

		if (supNeighbour < listCount)
			supNeighbourRefPointAngle = Vector3.Angle(list[supNeighbour] - currentPoint, referencePoint - currentPoint);
		else
			supNeighbour = -1;

		return infNeighbourRefPointAngle < supNeighbourRefPointAngle ? infNeighbour : supNeighbour;
	}

	int Mod(int a, int b)
	{
		int res = a % b;
		if (res < 0)
			res += b;
		return res;
	}

	// Return if any change was performed
	public bool DrawHandles(Event e)
	{
		bool needUpdate = false;
		for (int i = 0; i < terrain.points.Count; ++i)
		{
			Vector3 newPoint;
			Vector3 point = terrain.points[i];
			GUI.SetNextControlName(handlePath + i);
            Handles.color = Color.black;
            newPoint = Handles.FreeMoveHandle(point, Quaternion.identity, terrain.diskSize, Vector3.zero, Handles.CylinderHandleCap);
			if (newPoint != terrain.points[i])
			{
                if (e.modifiers == EventModifiers.Control)
                {
                    newPoint = terrain.transform.TransformPoint(newPoint);
                    newPoint.x = Handles.SnapValue(newPoint.x, terrain.snap.x);
                    newPoint.y = Handles.SnapValue(newPoint.y, terrain.snap.y);
                    newPoint = terrain.transform.InverseTransformPoint(newPoint);
                }

                Undo.RecordObject(terrain, "Terrain points modified");
				terrain.points[i] = newPoint;
				needUpdate = true;
			}

            Handles.color = Color.white;
			if (i != 0)
                Handles.DrawLine(terrain.points[i - 1], newPoint);
		}

		return needUpdate;
	}

	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();

		if (GUILayout.Button("Clear"))
		{
			Clear();
		}

		if (terrain.loop != isLooping)
		{
			isLooping = terrain.loop;
			OnEditorChanged();
		}
	}
}
