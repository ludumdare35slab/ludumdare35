﻿using UnityEditor;
using System.Collections;

public class CreateScriptableObject : Editor
{

	[MenuItem("Assets/Create/World/ShapeItem")]
	static void CreateActorData()
	{
		AssetDatabase.CreateAsset(new ShapeItemObject(), AssetDatabase.GetAssetPath(Selection.activeObject) + "/ShapeItem.asset");
	}
}
