﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

[CustomEditor(typeof(FPSInfo))]
public sealed class FPSInfoEditor : DebugInfoEditor
{
    #region FIELDS
    SerializedProperty m_fpsMaxLimit = null;
    SerializedProperty m_limitAverageCount = null;
    SerializedProperty m_timeCalculationMode = null;
    SerializedProperty m_warningLimit = null;
    SerializedProperty m_criticalLimit = null;
    SerializedProperty m_overrideTextColor = null;
    SerializedProperty m_normalColor = null;
    SerializedProperty m_warningColor = null;
    SerializedProperty m_criticalColor = null;
    #endregion

    #region EVENTS
    new void OnEnable()
    {
        base.OnEnable();

        m_fpsMaxLimit = m_serializedObject.FindProperty("m_fpsMaxLimit");
        m_limitAverageCount = m_serializedObject.FindProperty("m_limitAverageCount");
        m_timeCalculationMode = m_serializedObject.FindProperty("m_timeCalculationMode");
        m_warningLimit = m_serializedObject.FindProperty("m_warningLimit");
        m_criticalLimit = m_serializedObject.FindProperty("m_criticalLimit");
        m_overrideTextColor = m_serializedObject.FindProperty("m_overrideTextColor");
        m_normalColor = m_serializedObject.FindProperty("m_normalColor");
        m_warningColor = m_serializedObject.FindProperty("m_warningColor");
        m_criticalColor = m_serializedObject.FindProperty("m_criticalColor");
    }

    public override void OnInspectorGUI()
    {
        m_serializedObject.Update();

        base.OnInspectorGUI();

        EditorGUILayout.Space();
        EditorGUILayout.LabelField("FPS Info Settings", EditorStyles.boldLabel);
        
        EditorGUILayout.PropertyField(m_fpsMaxLimit, new GUIContent("FPS Max Limit"));
        EditorGUILayout.PropertyField(m_limitAverageCount, new GUIContent("Limit Average Count"));
        EditorGUILayout.PropertyField(m_warningLimit, new GUIContent("Warning Limit"));
        EditorGUILayout.PropertyField(m_criticalLimit, new GUIContent("Critical Limit"));
        EditorGUILayout.PropertyField(m_timeCalculationMode, new GUIContent("Time Calculation Mode"));

        EditorGUILayout.Space();

        EditorGUILayout.PropertyField(m_overrideTextColor, new GUIContent("Override Text Color"));
        if (m_overrideTextColor.boolValue == true)
        {
            EditorGUILayout.PropertyField(m_normalColor, new GUIContent("Normal Color"));
            EditorGUILayout.PropertyField(m_warningColor, new GUIContent("Warning Color"));
            EditorGUILayout.PropertyField(m_criticalColor, new GUIContent("Critical Color"));
        }

        if (GUI.changed)
            m_serializedObject.ApplyModifiedProperties();
    }
    #endregion
}
