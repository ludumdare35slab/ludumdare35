﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(DeviceInfo))]
public sealed class DeviceInfoEditor : DebugInfoEditor
{
    #region EVENTS
    new void OnEnable()
    {
        base.OnEnable();
    }

    public override void OnInspectorGUI()
    {
        m_serializedObject.Update();

        base.OnInspectorGUI();

        if (GUI.changed)
            m_serializedObject.ApplyModifiedProperties();
    }
    #endregion
}

