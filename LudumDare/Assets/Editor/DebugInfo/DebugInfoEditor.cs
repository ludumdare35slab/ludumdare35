﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

[CustomEditor(typeof(DebugInfo), true)]
public class DebugInfoEditor : Editor
{
    #region FIELDS
    protected SerializedObject m_serializedObject = null;

    SerializedProperty m_updateInterval = null;
    SerializedProperty m_screenPos = null;
    SerializedProperty m_priorAnchors = null;
    SerializedProperty m_anchorRect = null;
    SerializedProperty m_textColor = null;
    #endregion

    #region EVENTS
    protected void OnEnable()
    {
        m_serializedObject = new SerializedObject(target);

        m_updateInterval = m_serializedObject.FindProperty("m_updateInterval");
        m_screenPos = m_serializedObject.FindProperty("m_screenPos");
        m_priorAnchors = m_serializedObject.FindProperty("m_priorAnchors");
        m_anchorRect = m_serializedObject.FindProperty("m_anchorRect");
        m_textColor = m_serializedObject.FindProperty("m_textColor");
    }

    public override void OnInspectorGUI()
    {
        EditorGUILayout.BeginVertical();
        {
            EditorGUILayout.Space();
            EditorGUILayout.LabelField("Common Settings", EditorStyles.boldLabel);

            EditorGUILayout.PropertyField(m_updateInterval, new GUIContent("Update Interval"));
            EditorGUILayout.PropertyField(m_screenPos, new GUIContent("ScreenPos"));
            EditorGUILayout.PropertyField(m_priorAnchors, new GUIContent("Prior Anchors"), true);
            EditorGUILayout.PropertyField(m_anchorRect, new GUIContent("Anchor Rect"));
            EditorGUILayout.PropertyField(m_textColor, new GUIContent("Text Color"));
        }
        EditorGUILayout.EndVertical();
    }
    #endregion

    #region METHODS
    protected void InitializeBase()
    {
        m_serializedObject = new SerializedObject(target);

        m_updateInterval = m_serializedObject.FindProperty("m_updateInterval");
        m_screenPos = m_serializedObject.FindProperty("m_screenPos");
        m_priorAnchors = m_serializedObject.FindProperty("m_priorAnchors");
        m_anchorRect = m_serializedObject.FindProperty("m_anchorRect");
        m_textColor = m_serializedObject.FindProperty("m_textColor");
    }
    #endregion
}
