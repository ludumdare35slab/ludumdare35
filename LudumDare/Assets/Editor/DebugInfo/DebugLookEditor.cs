﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(DebugLook))]
public sealed class DebugLookEditor : Editor
{
    #region FIELDS
    SerializedObject m_serializedObject = null;

    SerializedProperty m_padding = null;
    SerializedProperty m_applyOutline = null;
    SerializedProperty m_outlineColor = null;
    SerializedProperty m_outlineEffectDistance = null;
    SerializedProperty m_applyShadow = null;
    SerializedProperty m_shadowColor = null;
    SerializedProperty m_shadowEffectDistance = null;
    SerializedProperty m_textFont = null;
    SerializedProperty m_fontSize = null;
    SerializedProperty m_backgroundColor = null;
    #endregion

    #region EVENTS
    public void OnEnable()
    {
        m_serializedObject = new SerializedObject(target);

        m_padding = m_serializedObject.FindProperty("m_padding");
        m_applyOutline = m_serializedObject.FindProperty("m_applyOutline");
        m_outlineColor = m_serializedObject.FindProperty("m_outlineColor");
        m_outlineEffectDistance = m_serializedObject.FindProperty("m_outlineEffectDistance");
        m_applyShadow = m_serializedObject.FindProperty("m_applyShadow");
        m_shadowColor = m_serializedObject.FindProperty("m_shadowColor");
        m_shadowEffectDistance = m_serializedObject.FindProperty("m_shadowEffectDistance");
        m_textFont = m_serializedObject.FindProperty("m_textFont");
        m_fontSize = m_serializedObject.FindProperty("m_fontSize");
        m_backgroundColor = m_serializedObject.FindProperty("m_backgroundColor");
    }

    public override void OnInspectorGUI()
    {
        m_serializedObject.Update();

        EditorGUILayout.Space();
        EditorGUILayout.Space();

        EditorGUILayout.LabelField("Text Settings", EditorStyles.boldLabel);
        EditorGUILayout.PropertyField(m_padding, new GUIContent("Padding"));
        EditorGUILayout.PropertyField(m_applyOutline, new GUIContent("ApplyOutline"));
        if (m_applyOutline.boolValue == true)
        {
            EditorGUILayout.PropertyField(m_outlineColor, new GUIContent("Outline Color"));
            EditorGUILayout.PropertyField(m_outlineEffectDistance, new GUIContent("Outline Effect Distance"));
        }

                EditorGUILayout.PropertyField(m_applyShadow, new GUIContent("Apply Shadow"));
        if (m_applyShadow.boolValue == true)
        {
            EditorGUILayout.PropertyField(m_shadowColor, new GUIContent("Shadow Color"));

            EditorGUILayout.PropertyField(m_shadowEffectDistance, new GUIContent("Shadow Effect Distance"));
        }

        EditorGUILayout.Space();
        EditorGUILayout.Space();

        EditorGUILayout.LabelField("Font Settings", EditorStyles.boldLabel);
        EditorGUILayout.PropertyField(m_textFont, new GUIContent("Font"));
        EditorGUILayout.PropertyField(m_fontSize, new GUIContent("Font Size"));

        EditorGUILayout.Space();
        EditorGUILayout.Space();

        EditorGUILayout.LabelField("Background Settings", EditorStyles.boldLabel);
        EditorGUILayout.PropertyField(m_backgroundColor, new GUIContent("Background Color"));

        if (GUI.changed)
            m_serializedObject.ApplyModifiedProperties();
    }
    #endregion
}
