﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(MemoryInfo))]
public sealed class MemoryInfoEditor : DebugInfoEditor
{
    #region EVENTS
    new void OnEnable()
    {
        base.OnEnable();
    }

    public override void OnInspectorGUI()
    {
        m_serializedObject.Update();

        base.OnInspectorGUI();

        if (GUI.changed)
            m_serializedObject.ApplyModifiedProperties();
    }
    #endregion
}
