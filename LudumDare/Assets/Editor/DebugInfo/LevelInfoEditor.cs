﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(LevelInfo))]
public sealed class LevelInfoEditor : DebugInfoEditor
{
    #region FIELDS
    SerializedProperty m_polyCountMode = null;
    #endregion

    #region EVENTS
    new void OnEnable()
    {
        base.OnEnable();

        m_polyCountMode = m_serializedObject.FindProperty("m_polyCountMode");
    }

    public override void OnInspectorGUI()
    {
        m_serializedObject.Update();

        base.OnInspectorGUI();

        EditorGUILayout.Space();
        EditorGUILayout.LabelField("Level Info Settings", EditorStyles.boldLabel);

        EditorGUILayout.PropertyField(m_polyCountMode, new GUIContent("Poly Count Mode"));
        if (m_polyCountMode.enumValueIndex == 1)
        {
            EditorGUILayout.HelpBox("Use 'AddMeshToSceneContainer(GameObject)' and 'RemoveMeshFromSceneContainer(GameObject)' each time you instantiate/destroy an object using a mesh renderer component. More efficient in scenes displaying a lot of Game Objects.", MessageType.Info);
        }

        if (GUI.changed)
            m_serializedObject.ApplyModifiedProperties();
    }
    #endregion
}
