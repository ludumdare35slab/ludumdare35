﻿#if UNITY_5_0 || UNITY_5_0_1 || UNITY_5_0_2 || UNITY_5_0_3 || UNITY_5_0_4 || UNITY_5_1 || UNITY_5_1_1 || UNITY_5_1_2 || UNITY_5_1_3 || UNITY_5_1_4 || UNITY_5_2 || UNITY_5_2_1 || UNITY_5_2_2 || UNITY_5_2_3 || UNITY_5_2_4
    #define OLD_UNITY
#endif

using UnityEngine;
using UnityEngine.UI;
#if !OLD_UNITY 
    using UnityEngine.SceneManagement;
#endif
using System.Threading;
using System.Collections;
using System.Collections.Generic;

public sealed class LevelInfo : DebugInfo
{
#region FIELDS
    [SerializeField]
    [Tooltip("The method used to calculate the polygons of each model on the scene.")]
    PolyCalculationMode m_polyCountMode = PolyCalculationMode.Auto;

	Text m_sceneNameText = null;
	Text m_objText = null;
	Text m_trisText = null;
	Text m_vertText = null;
    #endregion

    #region ENUMS
    public enum PolyCalculationMode : byte
    {
        Auto = 0,
        Manual
    }
#endregion

#region PROPERTIES
    public string SceneName { get; set; }
    public uint TriCount { get; set; }
    public uint VertCount { get; set; }
    public PolyCalculationMode PolyCountMode { get { return m_polyCountMode; } }
    public List<GameObject> SceneObjects { get; private set; }
#endregion

#region EVENTS
	void Awake()
	{
        m_texts = GetComponentsInChildren<Text>();

		m_sceneNameText = m_texts[0];
		m_objText = m_texts[1];
		m_trisText = m_texts[2];
		m_vertText = m_texts[3];

#if !OLD_UNITY
        SceneName = SceneManager.GetActiveScene().name;
		SceneManager.sceneLoaded += OnLevelLoaded;
#else
        SceneName = Application.loadedLevelName;
#endif
        SceneObjects = new List<GameObject>();

        if (PolyCountMode == PolyCalculationMode.Manual)
            FillSceneContainer();
    }

	void OnDestroy()
	{
#if !OLD_UNITY
		SceneManager.sceneLoaded -= OnLevelLoaded;
#endif
	}

#if OLD_UNITY
	void OnLevelWasLoaded(int scene)
	{
		OnLevelLoaded();
	}
#else
	void OnLevelLoaded(Scene s, LoadSceneMode l)
	{
		OnLevelLoaded();
	}
#endif

	void OnLevelLoaded()
	{
		if (PolyCountMode == PolyCalculationMode.Manual)
            FillSceneContainer();
    }
#endregion

#region METHODS
    public override IEnumerator CustomUpdate_Coroutine()
	{
        if (PolyCountMode == PolyCalculationMode.Auto)
            FillSceneContainer();

        m_sceneNameText.text = string.Format("SCENE: \"{0}\"", SceneName);
		m_objText.text = string.Format(Thread.CurrentThread.CurrentCulture, "OBJ: {0:n0}", SceneObjects.Count);
        m_trisText.text = string.Format(Thread.CurrentThread.CurrentCulture, "TRIS: {0:n0}", TriCount);
		m_vertText.text = string.Format(Thread.CurrentThread.CurrentCulture, "VERTS: {0:n0}", VertCount);

		yield return new WaitForSeconds(m_updateInterval);
		
		StartCoroutine(CustomUpdate_Coroutine());

        yield return null;
    }

    public void AddMeshToSceneContainer(GameObject obj)
    {
        SceneObjects.Add(obj);

        Transform[] transforms = obj.transform.GetComponentsInChildren<Transform>();
        for (ushort i = 0; i < transforms.Length; ++i)
        {
            GameObject child = transforms[i].gameObject;
            SceneObjects.Add(child);
            MeshFilter meshFilter = child.GetComponent<MeshFilter>();
            if (meshFilter != null)
            {
                TriCount += (uint)meshFilter.sharedMesh.triangles.Length;
                VertCount += (uint)meshFilter.sharedMesh.vertices.Length;
            }
        }
    }

    public void RemoveMeshFromSceneContainer(GameObject obj)
    {
        Transform[] transforms = obj.transform.GetComponentsInChildren<Transform>();
        for (uint i = 0; i < transforms.Length; ++i)
        {
            GameObject child = transforms[i].gameObject;
            MeshFilter meshFilter = child.GetComponent<MeshFilter>();
            if (meshFilter != null)
            {
                TriCount -= (uint)meshFilter.sharedMesh.triangles.Length;
                VertCount -= (uint)meshFilter.sharedMesh.vertices.Length;
            }

            SceneObjects.Remove(child);
        }

        SceneObjects.Remove(obj);
    }

    void FillSceneContainer()
    {
        GameObject[] gameObjects = GameObject.FindObjectsOfType(typeof(GameObject)) as GameObject[];
        if (gameObjects != null)
        {
            if (PolyCountMode == PolyCalculationMode.Auto)
            {
                TriCount = 0u;
                VertCount = 0u;

                SceneObjects.Clear();
            }

            for (uint i = 0u; i < gameObjects.Length; ++i)
            {
                SceneObjects.Add(gameObjects[i]);
                MeshFilter meshFilter = gameObjects[i].GetComponent<MeshFilter>();
                if (meshFilter != null)
                {
                    TriCount += (uint)meshFilter.sharedMesh.triangles.Length;
                    VertCount += (uint)meshFilter.sharedMesh.vertices.Length;
                }
            }
        }
    }
#endregion
}
