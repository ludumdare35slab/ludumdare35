﻿using UnityEngine;
using UnityEngine.UI;
using System.Threading;
using System.Collections;
using System.Collections.Generic;

public class DeviceInfo : DebugInfo 
{
	#region FIELDS
	Text m_deviceNameText = null;
	Text m_osText = null;
	Text m_cpuText = null;
	Text m_gpuText = null;
	Text m_ramText = null;
	Text m_screenText = null;
	#endregion
	
	#region EVENTS
	void Awake()
	{
        m_texts = GetComponentsInChildren<Text>();

        m_deviceNameText = m_texts[0];
		m_osText = m_texts[1];
		m_cpuText = m_texts[2];
		m_gpuText = m_texts[3];
		m_ramText = m_texts[4];
		m_screenText = m_texts[5];

		m_deviceNameText.text = string.Format("DEVICE NAME: {0}", SystemInfo.deviceName);
		m_osText.text = string.Format("OS: {0}", SystemInfo.operatingSystem);
		m_cpuText.text = string.Format("CPU: {0}, [{1} cores]", SystemInfo.deviceModel, SystemInfo.processorCount);
		m_gpuText.text = string.Format("GPU: {0}, API: {1}", SystemInfo.graphicsDeviceName, SystemInfo.graphicsDeviceType);

		m_ramText.text = string.Format(Thread.CurrentThread.CurrentCulture, "RAM: {0:n0} MB, VRAM: {1:n0} MB", SystemInfo.systemMemorySize, SystemInfo.graphicsMemorySize);
    }
	#endregion
	
	#region METHODS
	public override IEnumerator CustomUpdate_Coroutine()
	{
		m_screenText.text = string.Format("SCR: {0} [window size: {1}x{2}, DPI: {3}]", Screen.currentResolution, Screen.width, Screen.height, Screen.dpi);
		
		yield return new WaitForSeconds(m_updateInterval);
	
		StartCoroutine(CustomUpdate_Coroutine());

        yield return null;
    }
	#endregion
}
