﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
[RequireComponent(typeof (RectTransform), typeof(Image), typeof(CanvasRenderer))]
public abstract class DebugInfo : MonoBehaviour 
{
    #region FIELDS
    [SerializeField][Range(0f, 10f)]
    [Tooltip("The interval (in seconds) between each component update.")]
    protected float m_updateInterval = 0.1f;
    [SerializeField]
    [Tooltip("The position on screen on which the data will be anchored.")]
    protected ScreenAnchor m_screenPos = ScreenAnchor.TopLeft;
    [SerializeField]
    [Tooltip("The data (in order of the list) which will be displayed above the current one if anchored on the same position on screen.")]
    protected List<DebugInfo> m_priorAnchors = null;
    [SerializeField]
    [Tooltip("The object used to anchor the data on screen.")]
    protected RectTransform m_anchorRect = null;
    [SerializeField]
    [Tooltip("The color of the text.")]
    Color m_textColor = Color.black;

    protected Text[] m_texts = null;
    #endregion

    #region ENUMS
    public enum ScreenAnchor : byte
    {
        TopLeft = 0,
        TopCenter,
        TopRight,
        BottomLeft,
        BottomCenter,
        BottomRight
    }
    #endregion

    #region
    public Vector2 PaddingDirection { get; private set; }
    public Text[] Texts { get { return m_texts; } }
    public RectTransform AnchorRect { get { return m_anchorRect; } }
    public ScreenAnchor ScreenPos { get { return m_screenPos; } }
    #endregion

    #region EVENTS
    protected virtual void Start()
	{
        for (byte i = 0; i < m_texts.Length; ++i)
            m_texts[i].color = m_textColor;

        StartCoroutine(CustomUpdate_Coroutine());
	}
    #endregion

    #region METHODS
    public abstract IEnumerator CustomUpdate_Coroutine();

    public void SetPos(RectTransform rectTransform)
    {
        System.Action<TextAnchor> SetTextAlignment = delegate (TextAnchor textAnchor)
        {
            for (byte i = 0; i < m_texts.Length; ++i)
                m_texts[i].alignment = textAnchor;
        };

        System.Action SnapRectToAnchor = delegate ()
        {
            if (m_priorAnchors.Count != 0)
            {
                for (int i = m_priorAnchors.Count - 1; i >= 0; --i)
                {
                    if (m_priorAnchors[i].gameObject.activeSelf == true && ScreenPos == m_priorAnchors[i].ScreenPos)
                    {
                        rectTransform.position = m_priorAnchors[i].AnchorRect.position;
                        break;
                    }
                }
            }
        };

        switch (m_screenPos)
        {
            case ScreenAnchor.TopLeft:
                PaddingDirection = new Vector2(1, 1);

                rectTransform.anchorMin = new Vector2(0f, 1f);
                rectTransform.anchorMax = new Vector2(0f, 1f);
                rectTransform.pivot = new Vector2(0f, 1f);
                rectTransform.anchoredPosition = Vector2.zero;

                AnchorRect.anchorMin = new Vector2(0f, 0f);
                AnchorRect.anchorMax = new Vector2(0f, 0f);
                AnchorRect.anchoredPosition = Vector2.zero;

                SetTextAlignment(TextAnchor.MiddleLeft);
                SnapRectToAnchor();
                break;
            case ScreenAnchor.TopCenter:
                PaddingDirection = new Vector2(0, 1);

                rectTransform.anchorMin = new Vector2(0.5f, 1f);
                rectTransform.anchorMax = new Vector2(0.5f, 1f);
                rectTransform.pivot = new Vector2(0.5f, 1f);
                rectTransform.anchoredPosition = Vector2.zero;

                AnchorRect.anchorMin = new Vector2(0.5f, 0f);
                AnchorRect.anchorMax = new Vector2(0.5f, 0f);
                AnchorRect.anchoredPosition = Vector2.zero;

                SetTextAlignment(TextAnchor.MiddleCenter);
                SnapRectToAnchor();
                break;
            case ScreenAnchor.TopRight:
                PaddingDirection = new Vector2(-1, 1);

                rectTransform.anchorMin = new Vector2(1f, 1f);
                rectTransform.anchorMax = new Vector2(1f, 1f);
                rectTransform.pivot = new Vector2(1f, 1f);
                rectTransform.anchoredPosition = Vector2.zero;

                AnchorRect.anchorMin = new Vector2(1f, 0f);
                AnchorRect.anchorMax = new Vector2(1f, 0f);
                AnchorRect.anchoredPosition = Vector2.zero;

                SetTextAlignment(TextAnchor.MiddleRight);
                SnapRectToAnchor();
                break;
            case ScreenAnchor.BottomLeft:
                PaddingDirection = new Vector2(1, -1);

                rectTransform.anchorMin = new Vector2(0f, 0f);
                rectTransform.anchorMax = new Vector2(0f, 0f);
                rectTransform.pivot = new Vector2(0f, 0f);
                rectTransform.anchoredPosition = Vector2.zero;

                AnchorRect.anchorMin = new Vector2(0f, 0f);
                AnchorRect.anchorMax = new Vector2(0f, 0f);
                AnchorRect.anchoredPosition = new Vector2(0, rectTransform.rect.height);

                SetTextAlignment(TextAnchor.MiddleLeft);
                SnapRectToAnchor();
                break;
            case ScreenAnchor.BottomCenter:
                PaddingDirection = new Vector2(0, -1);

                rectTransform.anchorMin = new Vector2(0.5f, 0f);
                rectTransform.anchorMax = new Vector2(0.5f, 0f);
                rectTransform.pivot = new Vector2(0.5f, 0f);
                rectTransform.anchoredPosition = Vector2.zero;

                AnchorRect.anchorMin = new Vector2(0.5f, 0f);
                AnchorRect.anchorMax = new Vector2(0.5f, 0f);
                AnchorRect.anchoredPosition = new Vector2(0f, rectTransform.rect.height);

                SetTextAlignment(TextAnchor.MiddleCenter);
                SnapRectToAnchor();
                break;
            case ScreenAnchor.BottomRight:
                PaddingDirection = new Vector2(-1, -1);

                rectTransform.anchorMin = new Vector2(1f, 0f);
                rectTransform.anchorMax = new Vector2(1f, 0f);
                rectTransform.pivot = new Vector2(1f, 0f);
                rectTransform.anchoredPosition = Vector2.zero;

                AnchorRect.anchorMin = new Vector2(1f, 0f);
                AnchorRect.anchorMax = new Vector2(1f, 0f);
                AnchorRect.anchoredPosition = new Vector2(0, rectTransform.rect.height);

                SetTextAlignment(TextAnchor.MiddleRight);
                SnapRectToAnchor();
                break;
            default:
                break;
        }
    }
    #endregion
}
