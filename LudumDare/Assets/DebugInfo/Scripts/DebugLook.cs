﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public sealed class DebugLook : MonoBehaviour
{
    #region FIELDS
    [SerializeField]
    [Tooltip("Padding applied on the data.")]
    Vector2 m_padding = Vector2.zero;
    [SerializeField]
    [Tooltip("If 'true', applies an outline on the data.")]
    bool m_applyOutline = false;
    [SerializeField]
    [Tooltip("Outline color (if outline applied).")]
    Color m_outlineColor = Color.black;
    [SerializeField]
    [Tooltip("Outline effect distance (if outline applied).")]
    Vector2 m_outlineEffectDistance = Vector2.zero;
    [SerializeField]
    [Tooltip("If 'true', applies a shadow on the data.")]
    bool m_applyShadow = false;
    [SerializeField]
    [Tooltip("Shadow color (if shadow applied).")]
    Color m_shadowColor = Color.black;
    [SerializeField]
    [Tooltip("Shadow effect distance (if shadow applied).")]
    Vector2 m_shadowEffectDistance = Vector2.zero;

    [SerializeField]
    [Tooltip("The font used for all the data.")]
    Font m_textFont = null;
    [SerializeField]
    [Tooltip("The size of the font.")]
    int m_fontSize = 10;

    [SerializeField]
    [Tooltip("The color of the background. ")]
    Color m_backgroundColor = Color.black;

    List<Text> m_texts = null;
    List<Outline> m_outlines = null;
    List<Shadow> m_shadows = null;

    List<DebugInfo> m_debugInfoList = null;
    #endregion

    #region PROPERTIES
    public Vector2 Padding { get { return m_padding; } set { m_padding = value; } }
    public Vector2 OutlineEffectDistance { get { return m_outlineEffectDistance; } set { m_outlineEffectDistance = value; } }
    public Vector2 ShadowEffectDistance { get { return m_shadowEffectDistance; } set { m_shadowEffectDistance = value; } }
    public Color OutlineColor { get { return m_outlineColor; } set { m_outlineColor = value; } }
    public Color ShadowColor { get { return m_shadowColor; } set { m_shadowColor = value; } }
    public Font TextFont { get { return m_textFont; } set { m_textFont = value; } }
    public bool ApplyOutline { get { return m_applyOutline; } set { m_applyOutline = value; } }
    public bool ApplyShadow { get { return m_applyShadow; } set { m_applyShadow = value; } }
    public int FontSize { get { return m_fontSize; } set { m_fontSize = value; } }
    public Color BackgroundColor { get { return m_backgroundColor; } set { m_backgroundColor = value; } }
    #endregion

    #region EVENTS
    void Awake()
    {
        m_texts = new List<Text>();
        m_outlines = new List<Outline>();
        m_shadows = new List<Shadow>();

        m_debugInfoList = new List<DebugInfo>();
    }

    void Start()
    {
        m_debugInfoList.AddRange(GetComponentsInChildren<DebugInfo>());
        foreach (DebugInfo info in m_debugInfoList)
        {
            info.GetComponent<Image>().color = m_backgroundColor;
            Text[] texts = info.GetComponentsInChildren<Text>();
            for (byte i = 0; i < texts.Length; ++i)
            {
                if (texts[i] == null || texts[i].enabled == false)
                {
                    Debug.LogError("DebugLook - Awake: Text component on id \"" + i + "\" is missing or deactivated. Please fix it up!");
                    continue;
                }

                if (texts[i].GetComponent<Outline>() == null || texts[i].GetComponent<Outline>().enabled == false)
                    Debug.LogError("DebugLook - Awake: Outline component on text \"" + texts[i] + "\" is missing or deactivated. Please fix it up!");

                if (GetShadowComponent(texts[i]) == null || GetShadowComponent(texts[i]).enabled == false)
                    Debug.LogError("DebugLook - Awake: Shadow component on text \"" + texts[i] + "\" is missing or deactivated. Please fix it up!");

                m_texts.Add(texts[i]);
                m_outlines.Add(texts[i].GetComponent<Outline>());
                m_shadows.Add(GetShadowComponent(texts[i]));
            }
        }

        UpdatePositionInfo();
        UpdateAppearanceInfo();
    }
    #endregion

    #region METHODS
    void UpdatePositionInfo()
    {
        for (byte i = 0; i < m_debugInfoList.Count; ++i)
        {
            DebugInfo debugInfo = m_debugInfoList[i];
            RectTransform debugInfoRect = debugInfo.GetComponent<RectTransform>();

            debugInfo.SetPos(debugInfoRect);
        }

        for (byte i = 0; i < m_debugInfoList.Count; ++i)
        {
            DebugInfo debugInfo = m_debugInfoList[i];
            RectTransform debugInfoRect = debugInfo.GetComponent<RectTransform>();

            debugInfoRect.anchoredPosition += new Vector2(m_padding.x * debugInfo.PaddingDirection.x, m_padding.y * debugInfo.PaddingDirection.y);
        }
    }

    void UpdateAppearanceInfo()
    {
        for (byte i = 0; i < m_texts.Count; ++i)
        {
            m_texts[i].font = m_textFont;
            m_texts[i].fontSize = m_fontSize;

            if (m_applyOutline == true)
            {
                m_outlines[i].enabled = true;
                m_outlines[i].effectColor = m_outlineColor;
                m_outlines[i].effectDistance = m_outlineEffectDistance;
            }
            else
            {
                if (m_outlines[i].enabled == true)
                    m_outlines[i].enabled = false;
            }

            if (m_applyShadow == true)
            {
                m_shadows[i].enabled = true;
                m_shadows[i].effectColor = m_shadowColor;
                m_shadows[i].effectDistance = m_shadowEffectDistance;
            }
            else
            {
                if (m_shadows[i].enabled == true)
                    m_shadows[i].enabled = false;
            }
        }
    }

    // This function fixes the ambiguous GetComponent<Shadow>() that bring bugs if used with an Outline.
    Shadow GetShadowComponent(Text text)
    {
        Outline outline = text.GetComponent<Outline>();
        Shadow[] trueShadows = text.GetComponents<Shadow>();
        Shadow trueShadow = null;
        for (byte i = 0; i < trueShadows.Length; ++i)
        {
            if (outline != null && trueShadows[i].GetInstanceID() == outline.GetInstanceID())
                continue;

            trueShadow = trueShadows[i];
        }

        return trueShadow;
    }
    #endregion
}
