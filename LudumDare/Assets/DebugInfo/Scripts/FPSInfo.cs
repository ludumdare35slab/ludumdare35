﻿using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;
using System;
using System.Linq;
using System.Threading;
using System.Collections;
using System.Collections.Generic;

public sealed class FPSInfo : DebugInfo
{
	#region FIELDS
	[SerializeField]
    [Tooltip("The limit number the framerate can show. Set up to 60 by default.")]
    ushort m_fpsMaxLimit = 60;
    [SerializeField]
    [Tooltip("The limit number used for the average calculation. Once reached, the older value is flushed from the process.")]
    ushort m_limitAverageCount = 10;
	[SerializeField]
    [Tooltip("The calculation mode used to get the time elapsed between two frames.")]
    TimeMode m_timeCalculationMode = TimeMode.Default;
    [SerializeField]
    [Tooltip("The warning limit number for the framerate. Set up at 30 by default.")]
    ushort m_warningLimit = 30;
    [SerializeField]
    [Tooltip("The critical limit number for the framerate. Set up at 20 by default.")]
    ushort m_criticalLimit = 20;

    [SerializeField]
    [Tooltip("If 'true', doesn't take into account the text color value and use 'normal color', 'warning color' and 'critical color' as new reference.")]
    bool m_overrideTextColor = false;
	[SerializeField]
    [Tooltip("Normal framerate color. Green by default.")]
    Color m_normalColor = Color.green;
	[SerializeField]
    [Tooltip("Warning framerate color. Yellow by default.")]
    Color m_warningColor = Color.yellow;
	[SerializeField]
    [Tooltip("Critical framerate color. Red by default.")]
    Color m_criticalColor = Color.red;

	ushort m_frameCount = 0;
    float m_count = 0f;
	float m_deltaTime = 0f;
	float m_elapsedTime = 0f;
	float m_minCount = 0f;
	float m_maxCount = 0f;
	float m_avgCount = 0f;
	List<float> m_counts = null;

	Text m_fpsText = null;
	Text m_minText = null;
	Text m_maxText = null;
	Text m_avgText = null;
	#endregion

	#region ENUMS
	public enum TimeMode : byte
	{
		Default = 0,
		Fixed,
		Smoothed,
		Unscaled
	}
	#endregion
	
	#region PROPERTIES
	public ushort FpsMaxLimit { get { return m_fpsMaxLimit; } }
	public float Count
    {
        get
        {
            return m_count;
        }
        private set
        {
            m_count = value;

            if (m_count > FpsMaxLimit)
                m_count = FpsMaxLimit;
        }
    }
	public float RenderTime { get; private set; }
	public TimeMode TimeCalculationMode { get { return m_timeCalculationMode; } set { m_timeCalculationMode = value; } }
    
    public bool OverrideTextColor { get { return m_overrideTextColor; } set { m_overrideTextColor = value; } }
    public Color NormalColor { get { return m_normalColor; } set { m_normalColor = value; } }
    public Color WarningColor { get { return m_warningColor; } set { m_warningColor = value; } }
    public Color CriticalColor { get { return m_criticalColor; } set { m_criticalColor = value; } }
    #endregion

    #region EVENTS
    void Awake()
	{
        m_texts = GetComponentsInChildren<Text>();

		m_fpsText = m_texts[0];
		m_minText = m_texts[1];
		m_maxText = m_texts[2];
		m_avgText = m_texts[3];

		m_counts = new List<float>();
	}

	void Update()
	{
		switch (TimeCalculationMode)
		{
		case TimeMode.Default:
			m_deltaTime = Time.deltaTime;
			break;
		case TimeMode.Fixed:
			m_deltaTime = Time.fixedDeltaTime;
			break;
		case TimeMode.Smoothed:
			m_deltaTime = Time.smoothDeltaTime;
			break;
		case TimeMode.Unscaled:
			m_deltaTime = Time.unscaledDeltaTime;
			break;
		default:
			break;
		}

		++m_frameCount;
		m_elapsedTime += m_deltaTime;
		
		if (m_elapsedTime >= m_updateInterval)
		{
			Count = m_frameCount / m_elapsedTime;
			SetMinMax();
			RenderTime = m_deltaTime * 1000f;
			m_frameCount = 0;
			m_elapsedTime = m_updateInterval - m_elapsedTime;
		}
	}
	#endregion

	#region METHODS
	public override IEnumerator CustomUpdate_Coroutine()
	{
		m_fpsText.text = string.Format(Thread.CurrentThread.CurrentCulture, "FPS: {0:0} ({1:n} ms)", Count, RenderTime);
		m_minText.text = string.Format("MIN: {0:0}", m_minCount);
		m_maxText.text = string.Format("MAX: {0:0}", m_maxCount);
		m_avgText.text = string.Format("AVG: {0:0}", m_avgCount = Mathf.Floor(CalculateAverageFPS(Count)));

		SetFramerateColor();
		
		yield return new WaitForSeconds(m_updateInterval);
		
		StartCoroutine(CustomUpdate_Coroutine());

        yield return null;
	}

	public void ResetAverage()
	{
		m_minCount = 0;
		m_maxCount = 0;
		m_avgCount = 0;
	}

	void SetFramerateColor()
	{
        if (m_overrideTextColor == true)
        {
		    Action<float, Text> SetTextColor = (float f, Text text) =>
		    {
			    if ((f <= FpsMaxLimit && f >= m_warningLimit) ||
			        f > FpsMaxLimit)
				    text.color = m_normalColor;
			    else if (f < m_warningLimit && f >= m_criticalLimit)
				    text.color = m_warningColor;
			    else if (f < m_criticalLimit)
				    text.color = m_criticalColor;
			    else
				    text.color = Color.black;
		    };

		    SetTextColor(Count, m_fpsText);
		    SetTextColor(m_minCount, m_minText);
		    SetTextColor(m_maxCount, m_maxText);
		    SetTextColor(m_avgCount, m_avgText);
        }
	}

	void SetMinMax()
	{
		if (m_minCount == 0)
			m_minCount = Count;

		if (Count > m_maxCount)
			m_maxCount = Count;

		if (Count < m_minCount)
			m_minCount = Count;
	}
	
	float CalculateAverageFPS(float currentCount)
	{
		float val = 0f;

		Func<float> ReturnAvgFPS = () =>
		{
			m_counts.Add(currentCount);
			if (m_counts.Count >= 2)
                return m_counts.Average();

			return currentCount;
		};

		if (m_counts != null)
		{
			if (m_counts.Count < m_limitAverageCount)
			{
				val = ReturnAvgFPS();
				return val;
			}
			else
			{
				m_counts.RemoveAt(0);
				val = ReturnAvgFPS();
				return val;
			}
		}

		Debug.LogError("FPSInfo - CalculateAverageFPS: counter list not found!");
		return -1f;
	}
	#endregion
}
