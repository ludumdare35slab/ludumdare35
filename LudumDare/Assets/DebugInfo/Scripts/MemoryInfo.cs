﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Threading;
using System.Collections;
using System.Collections.Generic;

public sealed class MemoryInfo : DebugInfo 
{
	#region FIELDS
	Text m_memSystemText = null;
	Text m_memVideoText = null;
	Text m_memAllocText = null;
	#endregion
	
	#region EVENTS
	void Awake()
	{
        m_texts = GetComponentsInChildren<Text>();

        m_memSystemText = m_texts[0];
        m_memVideoText = m_texts[1];
        m_memAllocText = m_texts[2];
    }
	#endregion
	
	#region METHODS
	public override IEnumerator CustomUpdate_Coroutine()
	{
        m_memSystemText.text = string.Format(Thread.CurrentThread.CurrentCulture, "MEM SYSTEM: {0:n0} MB", SystemInfo.systemMemorySize);
		m_memVideoText.text = string.Format(Thread.CurrentThread.CurrentCulture, "MEM VIDEO: {0:n0} MB", SystemInfo.graphicsMemorySize);
		m_memAllocText.text = string.Format(Thread.CurrentThread.CurrentCulture, "MEM ALLOC: {0:n0} MB", GC.GetTotalMemory(true) / 10000);
		
		yield return new WaitForSeconds(m_updateInterval);
		
		StartCoroutine(CustomUpdate_Coroutine());

        yield return null;
    }
	#endregion
}
