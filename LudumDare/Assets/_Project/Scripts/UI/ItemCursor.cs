﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ItemCursor : MonoBehaviour
{
	public List<ItemIcon> icons;
	Player player;

	void Start()
	{
		LevelController controller = GameObject.FindObjectOfType<LevelController>();
		if (!controller.PlayerSpawned)
			controller.OnPlayerSpawn.AddListener(GetPlayer);
		else
			GetPlayer();
		PointIcon(icons[0]);
	}

	void GetPlayer()
	{
		player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
		player.onShapeAdded += OnShapeAdded;
		player.onShapeSwap += OnShapeSwap;
	}

	void OnShapeSwap(ShapeItemObject newObject)
	{
		foreach (ItemIcon icon in icons)
		{
			if (icon.linkedObj == newObject)
				PointIcon(icon);
		}
	}

	void PointIcon(ItemIcon icon)
	{
		transform.rotation = icon.cursorPoint.transform.rotation;
		transform.position = icon.cursorPoint.transform.position;
	}

	void OnShapeAdded(ShapeItemObject newObject)
	{

	}
}
