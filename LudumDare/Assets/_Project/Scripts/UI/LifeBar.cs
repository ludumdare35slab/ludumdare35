﻿using UnityEngine;
using System.Collections;

public class LifeBar : MonoBehaviour
{
	public GameObject hearthPrefab;
	public GameObject emptyHeartPrefab;

	Player player;
	void Start()
	{
		LevelController controller = GameObject.FindObjectOfType<LevelController>();
		if (!controller.PlayerSpawned)
			controller.OnPlayerSpawn.AddListener(GetPlayer);
		else
			GetPlayer();
	}

	void GetPlayer()
	{
		player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
		player.onLifeChanged += OnLifeChanged;
		UpdateBar(player.life);
	}

	void OnLifeChanged(int newLife)
	{
		UpdateBar(newLife);
	}

	void UpdateBar(int hearthCount)
	{
		for (int i = 0; i < transform.childCount; ++i)
			Destroy(transform.GetChild(i).gameObject);

		for (int i = hearthCount; i < player.baseLife; ++i)
		{
			GameObject hearth = Instantiate(emptyHeartPrefab);
			hearth.transform.SetParent(transform, false);
		}
		for (int i = 0; i < hearthCount; ++i)
		{
			GameObject hearth = Instantiate(hearthPrefab);
			hearth.transform.SetParent(transform, false);
		}

	}
}
