﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OptionCanvas : MonoBehaviour
{
	bool _initializing = true;

	void Start ()
	{
		InputManager.Instance.Game.PauseDown += PauseGame;
		gameObject.SetActive(false);
	}

	void OnEnable()
	{
		if (_initializing)
			return;

		Time.timeScale = 0;
		InputManager.Instance.Menu.PauseDown += UnpauseGame;
	}

	void OnDisable()
	{
		if (_initializing)
		{
			_initializing = false;
			return;
		}
		Time.timeScale = 1;
		InputManager.Instance.Menu.PauseDown -= UnpauseGame;
	}

	public void PauseGame()
	{
		Device.Instance.nextState = Device.GameState.Menu;
	}

	public void UnpauseGame()
	{
		Device.Instance.nextState = Device.GameState.Level;
	}
}
