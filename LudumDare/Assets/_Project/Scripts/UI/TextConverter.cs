﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Text.RegularExpressions;

public class TextConverter : MonoBehaviour
{
	Text text;

	string originalText;

	void Start ()
	{
		text = GetComponent<Text>();
		originalText = text.text;
		text.text = ConvertText(originalText);
		InputManager.Instance.deviceChanged += OnDeviceTypeChanged;
	}

	string ConvertText(string text)
	{
		Regex regex = new Regex(@"\<(\w+)=(\w+)\>");
		MatchCollection matches = regex.Matches(text);
		int shifting = 0;
		foreach (Match match in matches)
		{
			switch (match.Groups[1].Value)
			{
				case "btn":
					string substitute = ConvertButton(match.Groups[2].Value);
					text = text.Remove(match.Index + shifting, match.Length).Insert(match.Index + shifting, substitute);
					shifting += substitute.Length - match.Length;
					break;
				default:
					Debug.LogWarning("Unknown command in \"" + text + "\"");
					break;
			} 
		}
		return text;
	}

	string ConvertButton(string buttonName)
	{
		switch (buttonName)
		{
			case "Line":
				return InputManager.Instance.GetLineButton();
			case "Triangle":
				return InputManager.Instance.GetTriangleButton();
			case "Magnet":
				return InputManager.Instance.GetMagnetButton();
			case "Hammer":
				return InputManager.Instance.GetHammerButton();
			default:
				Debug.LogWarning("Unrecognise button " + buttonName + " in \"" + text + "\"");
				return "";
		}
	}

	void OnDeviceTypeChanged(InputManager.DeviceType newType)
	{
		text.text = ConvertText(originalText);
	}
}
