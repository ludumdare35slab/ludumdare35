﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ItemIcon : MonoBehaviour
{
	[HideInInspector]
	public Player player;

	public ShapeItemObject linkedObj;
	public RectTransform cursorPoint;
	public Image image;
	public GameObject wheelSelected;
	public GameObject letter;

	public bool startSelected;

	[HideInInspector]
	public bool selected = false;

	void Start()
	{
		LevelController controller = GameObject.FindObjectOfType<LevelController>();
		if (!controller.PlayerSpawned)
			controller.OnPlayerSpawn.AddListener(GetPlayer);
		else
			GetPlayer();

		if (startSelected)
		{
			selected = true;
			image.sprite = linkedObj.iconNotSelected;
			ShapeAdded(linkedObj);
			SelectItem();
		}
		else
		{
			image.sprite = linkedObj.iconNotSelected;
		}
	}

	void GetPlayer()
	{
		player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
		player.onShapeAdded += ShapeAdded;
		player.onShapeSwap += ShapeSwap;
	}

	void ShapeAdded(ShapeItemObject newObject)
	{
		if (newObject == linkedObj)
		{
			image.enabled = true;
			image.sprite = linkedObj.iconNotSelected;
			letter.SetActive(true);
		}
	}

	void SelectItem()
	{
		selected = true;
		image.sprite = linkedObj.iconSelected;
		wheelSelected.SetActive(true);
		letter.SetActive(false);
	}

	void UnselectItem()
	{
		selected = false;
		wheelSelected.SetActive(false);
		image.sprite = linkedObj.iconNotSelected;
		letter.SetActive(true);
	}

	void ShapeSwap(ShapeItemObject newObject)
	{
		if (newObject == linkedObj && !selected)
			SelectItem();
		else if (selected)
			UnselectItem();
	}
}
