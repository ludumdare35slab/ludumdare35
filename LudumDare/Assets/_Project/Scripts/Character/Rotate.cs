﻿using UnityEngine;
using System.Collections;

public class Rotate : MonoBehaviour
{
	public float rotateSpeed;

	[HideInInspector]
	public float rotationDir = -1;
	[HideInInspector]
	public float pauseRotation = 1;

	delegate void FloatDelegate(float newRotationDir);

	FloatDelegate onRotationDirChanged;

	public void PauseRotation(float time)
	{
		StartCoroutine(PauseRotationCoroutine(time));
	}

	IEnumerator PauseRotationCoroutine(float time)
	{
		pauseRotation = 0;
		yield return new WaitForSeconds(time); 
		pauseRotation = 1;
	}

	void Update ()
	{
		transform.Rotate(new Vector3(0, 0, rotateSpeed) * Time.deltaTime * rotationDir * pauseRotation);
	}
}
