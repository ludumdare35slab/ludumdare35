﻿using UnityEngine;
using System.Collections;

public class MouseMove : MonoBehaviour
{
	public float mouseSensibility;

	[HideInInspector]
	public CursorLockMode lockMode = CursorLockMode.Locked;

	void Update ()
	{
		if (Input.GetMouseButtonDown(0))
			Cursor.lockState = lockMode;

		if (Cursor.lockState == lockMode)
			transform.localPosition += new Vector3(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y")) * mouseSensibility * Time.deltaTime;
	}
}
