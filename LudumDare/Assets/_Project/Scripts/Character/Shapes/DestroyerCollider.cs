﻿using UnityEngine;
using System.Collections;

public class DestroyerCollider : MonoBehaviour
{
	void OnCollisionEnter2D(Collision2D collision)
	{
		if (collision.gameObject.CompareTag("DestroyableBlock"))
			collision.gameObject.GetComponent<DestroyableObject>().Destroy();
	}
} 
