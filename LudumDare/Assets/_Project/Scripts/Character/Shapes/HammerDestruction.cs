﻿using UnityEngine;
using System.Collections;

public class HammerDestruction : MonoBehaviour
{
	Player player;

	public GameObject colliderLeft;
	public GameObject colliderRight;

	float prevDir;

	void Start()
	{
		LevelController controller = GameObject.FindObjectOfType<LevelController>();
		if (!controller.PlayerSpawned)
			controller.OnPlayerSpawn.AddListener(GetPlayer);
		else
			GetPlayer();
	}

	void GetPlayer()
	{
		player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
		ComputeDirection();
	}

	void Update ()
	{
		if (player.rotate.rotationDir != prevDir)
			ComputeDirection();
	}

	void ComputeDirection()
	{
		prevDir = player.rotate.rotationDir;
	}
}
