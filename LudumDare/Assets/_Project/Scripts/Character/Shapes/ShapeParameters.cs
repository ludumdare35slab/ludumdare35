﻿using UnityEngine;
using System.Collections;

public class ShapeParameters : MonoBehaviour {

	public float shapeSpeed;
	public ShapeItemObject obj;

	void Start ()
	{
		transform.parent.GetComponent<Rotate>().rotateSpeed = shapeSpeed;
	}
}
