﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
public class Player : MonoBehaviour
{
	public int baseLife;
	public ShapeItemObject basicShape;
	public GameObject newShapeFeedbackPrefab;
	public GameObject shapeParentGO;
	public AudioClip hitSound;
	public AudioClip changeFormSound;
	public AudioClip deathSound;
	public List<GameObject> deathParticles;

	[Space(10)]
	public SpriteRenderer pivotSprite;
	public List<Sprite> lifeAmountDisplay;

	[Space(10)]
	public bool needReset;
	public float invulnerabilityTimeOnHit = 0.5f;
	public float respawnCooldown = 1;

	public float shapeChangeCooldown = 0.5f;
	public float pauseRotationTimeOnDamage = 0.3f;
	public float onDamageTranslation = 1f;
	public float onDamageRotation = 50f;
	[Space(10)]
	public bool resetShapeCurrentOnRespawn = true;

	public delegate void ShapeDelegate(ShapeItemObject item);
	public delegate void LifeDelegate(int newLifeCount);
	[HideInInspector]
	public bool inShapeChangeCooldown;
	[HideInInspector]
	public ShapeDelegate onShapeAdded;
	[HideInInspector]
	public ShapeDelegate onShapeSwap;
	[HideInInspector]
	public LifeDelegate onLifeChanged;
	[HideInInspector]
	public Rotate rotate;
	[HideInInspector]
	public Respawner respawner;
	[HideInInspector]
	public PlayerCollisions collisionsHandler;
	[HideInInspector]
	public int life;
	[HideInInspector]
	public List<ShapeItemObject> shapes;
	[HideInInspector]
	public int currentShapeID = -1;
	[HideInInspector]
	public GameObject shapeGO;
	[HideInInspector]
	public int currentShapePreviewID = -1;
	[HideInInspector]
	public GameObject previewShapeGO;
	[HideInInspector]
	public Animator shapeAnimator;
	[HideInInspector]
	public KeyboardMove keyboardMove;
	[HideInInspector]
	public bool invulnerable;

	[HideInInspector]
	bool hasControl = true;

	void Awake()
	{
		respawner = GetComponent<Respawner>();
		rotate = GetComponent<Rotate>();
		keyboardMove = GetComponent<KeyboardMove>();
		collisionsHandler = GetComponent<PlayerCollisions>();
        SetControls(true);
		shapes.Add(basicShape);
		SwapShape(0);
		SetLife(baseLife, false);
		onLifeChanged += TestDeath;
		onShapeAdded += delegate { SpawnParticles(newShapeFeedbackPrefab, false); };
	}

	void Start()
	{
		InputManager.Instance.Game.TriangleDown += OnShapeButtonDown;
		InputManager.Instance.Game.LineDown += OnShapeButtonDown;
		InputManager.Instance.Game.HammerDown += OnShapeButtonDown;
		InputManager.Instance.Game.MagnetDown += OnShapeButtonDown;
		InputManager.Instance.Game.TriangleUp += OnShapeButtonUp;
		InputManager.Instance.Game.LineUp += OnShapeButtonUp;
		InputManager.Instance.Game.HammerUp += OnShapeButtonUp;
		InputManager.Instance.Game.MagnetUp += OnShapeButtonUp;

		onLifeChanged += ChangePivotSprite;
	}

	void ChangePivotSprite(int newLifeCount)
	{
		if (newLifeCount > 0)
			pivotSprite.sprite = lifeAmountDisplay[(int)(newLifeCount / (float)baseLife * 3) - 1];
	}

	void SpawnParticles(GameObject prefab, bool followPlayer)
	{
		if (prefab)
		{
			GameObject obj = Instantiate(prefab, transform.position, Quaternion.identity) as GameObject;
			if (followPlayer)
				obj.transform.SetParent(transform);
		}
	}

	public ShapeItemObject GetCurrentShape()
	{
		return shapes[currentShapeID];
	}

	public void Hit()
	{
		SetLife(life - 1);

		StartInvulnerability();
		if (life > 0)
		{
			FxManager.Instance.RShakeCamera(0.2f, 0.1f);
			if (shapeAnimator)
				shapeAnimator.SetTrigger("Hit");
		}
	}

	public void StopControlsOverTime(float time)
	{
		StartCoroutine(StopControlsCoroutine(time));
	}

    public void SetControls(bool activated)
    {
		if (activated)
		{
			hasControl = true;
			keyboardMove.enabled = true;
		}
		else
		{
			hasControl = false;
			keyboardMove.enabled = false;
		}
    }

	IEnumerator StopControlsCoroutine(float time)
	{
		SetControls(false);
		yield return new WaitForSeconds(time);
        SetControls(true);
	}

	public void StartInvulnerability()
	{
		StartCoroutine(InvulnerabilityCoroutine());
	}

	public IEnumerator InvulnerabilityCoroutine()
	{
		invulnerable = true;
		yield return new WaitForSeconds(invulnerabilityTimeOnHit);
		invulnerable = false;
	}

	public void SetLife(int newLifeCount, bool animate = true)
	{
		if (onLifeChanged != null)
			onLifeChanged(newLifeCount);
        if (animate && newLifeCount > life)
			shapeAnimator.SetTrigger("Heal");
        life = newLifeCount;
	}

	public void SwapShape(int shapeID, bool immediate = false)
	{
		if (shapeID >= shapes.Count || shapeID == currentShapeID)
			return;
		UnpreviewShape();
		currentShapeID = shapeID;
		if (shapeGO)
			ClearShape(immediate);
		UseShape(shapes[currentShapeID]);
		if (onShapeSwap != null)
			onShapeSwap(shapes[currentShapeID]);
		StartCoroutine(SwapCooldownCoroutine(shapeChangeCooldown));
	}

	IEnumerator SwapCooldownCoroutine(float time)
	{
		inShapeChangeCooldown = true;
		yield return new WaitForSeconds(time);
		inShapeChangeCooldown = false;
	}

	public ShapeItemObject GetShapeByName(string name)
	{
		int res = GetShapeIDByName(name);
		if (res == -1)
			return null;
		else
			return shapes[res];
	}

	public int GetShapeIDByName(string name)
	{
		int i = 0;
		foreach (ShapeItemObject obj in shapes)
		{
			if (obj.name == name)
			{
				return i;
			}
			++i;
		}
		return -1;
	}

	public void SwapShape(string name, bool immediate = false)
	{
		int id = GetShapeIDByName(name);
		if (id == -1)
			return;
		SwapShape(id, immediate);
	}

	void PreviewShape(string name)
	{
		int id = GetShapeIDByName(name);
		if (id == -1 || id == currentShapeID)
			return;

		UnpreviewShape();

		if (shapes[id].previewPrefab)
		{
			previewShapeGO = Instantiate(shapes[id].previewPrefab);
			previewShapeGO.transform.SetParent(transform, false);

			currentShapePreviewID = id;
		}
	}

	void UnpreviewShape()
	{
		if (previewShapeGO)
			Destroy(previewShapeGO);
		currentShapePreviewID = -1;
	}

	void ClearShape(bool immediate = false)
	{
		if (shapeAnimator && !immediate)
			shapeAnimator.SetTrigger("Disapear");
		else
			Destroy(shapeGO);

		shapeGO = null;
	}

	void UseShape(ShapeItemObject newShape)
	{
		shapeGO = Instantiate(newShape.prefab);
		shapeGO.transform.SetParent(shapeParentGO.transform, false);
		shapeAnimator = shapeGO.GetComponent<Animator>();
		if (changeFormSound)
			SoundManager.Instance.PlaySound2D(changeFormSound);
	}

	public void AddShape(ShapeItemObject shapeScriptableObject)
	{
		shapes.Add(shapeScriptableObject);
		if (onShapeAdded != null)
			onShapeAdded(shapeScriptableObject);
        shapeAnimator.SetTrigger("NewShape");
    }

    public void Reset()
	{
		SetLife(baseLife, false);
		transform.rotation = Quaternion.identity;
		needReset = false;
	}

	void TestDeath(int life)
	{
		if (life <= 0)
			Die();
	}

	void OnShapeButtonUp(string name)
	{
		if (hasControl && !inShapeChangeCooldown)
		{
			int shapeId = GetShapeIDByName(name);
			if (currentShapePreviewID == -1 || shapeId == currentShapePreviewID)
				SwapShape(name);
		}
	}

	void OnShapeButtonDown(string name)
	{
		if (hasControl && !inShapeChangeCooldown)
			PreviewShape(name);
	}

	void Update ()
	{
		if (needReset)
			Reset();
	}

	IEnumerator RespawnCooldownCoroutine(float time)
	{

		foreach(GameObject deathParticle in deathParticles)
			Instantiate(deathParticle, transform.position, Quaternion.identity);
		rotate.PauseRotation(time);
		SetControls(false);
		for (int i = 0; i < transform.childCount; ++i)
			transform.GetChild(i).gameObject.SetActive(false);

		yield return new WaitForSeconds(time);

		for (int i = 0; i < transform.childCount; ++i)
		{
			transform.GetChild(i).gameObject.SetActive(true);
			GameObject childObj = transform.GetChild(i).gameObject;
			ShapeParameters param = childObj.GetComponent<ShapeParameters>();
			if (param)
				Destroy(childObj);
			currentShapeID = -1;	
		}
		needReset = true;
		SwapShape(0, true);
		respawner.Respawn();
		SetControls(true);
	}

	void Die()
	{
		if (deathSound)
			SoundManager.Instance.PlaySound2D(deathSound);
		StartCoroutine(RespawnCooldownCoroutine(respawnCooldown));
	}
}
