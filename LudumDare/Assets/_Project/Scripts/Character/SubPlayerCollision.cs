﻿using UnityEngine;
using System.Collections;

public class SubPlayerCollision : MonoBehaviour
{
	[HideInInspector]
	public Player player;

	public bool rotationsReversed;

	void Start()
	{
		player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
	}

	void OnCollisionStay2D(Collision2D collision)
	{
		if (!collision.gameObject.CompareTag("ActionItem"))
		{
			if (!player.invulnerable)
			{
				player.Hit();
				if (player.life > 0)
				{
					player.rotate.PauseRotation(player.pauseRotationTimeOnDamage);
					player.StopControlsOverTime(player.pauseRotationTimeOnDamage);
				}
			}

			int dir = -1;
			if (rotationsReversed)
				dir = 1;

			if (!player.collisionsHandler.collisionThisFixedFrame)
			{
				player.collisionsHandler.collisionThisFixedFrame = true;
				Vector2 moveDir = (player.transform.worldToLocalMatrix * collision.contacts[0].normal).normalized;
				player.transform.Translate(moveDir * player.onDamageTranslation);
				player.transform.Rotate(0, 0, player.onDamageRotation * dir);
				if (player.hitSound)
					SoundManager.Instance.PlaySound2D(player.hitSound);
			}
		}
	}

	void OnCollisionEnter2D(Collision2D collision)
	{
        GameObject obj = collision.gameObject;
        if (obj.CompareTag("ActionItem"))
        {
            ActionItem actionItem = gameObject.GetComponent<ActionItem>();

            if (actionItem)
                actionItem.OnAction(player);
        }
	}
}
