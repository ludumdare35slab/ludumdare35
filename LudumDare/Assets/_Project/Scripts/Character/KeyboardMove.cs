﻿using UnityEngine;
using System.Collections;

public class KeyboardMove : MonoBehaviour
{
	//public float descelerationRate = 0;
	public float maxMoveSpeed = 2;

	[HideInInspector]
	public Vector2 speed;
	[HideInInspector]
	public Vector2 acceleration;

	void Start ()
	{
		InputManager.Instance.Game.Horizontal += UpdateX;
		InputManager.Instance.Game.Vertical += UpdateY;
	}

	void UpdateX(float axis)
	{
		speed.x = axis;
	}

	void UpdateY(float axis)
	{
		speed.y = axis;
	}

	void FixedUpdate ()
	{
		if (speed.magnitude > 1)
			speed.Normalize();
		speed *= maxMoveSpeed;

		transform.localPosition += new Vector3(speed.x, speed.y, 0) * Time.fixedDeltaTime;
	}
}
