﻿using UnityEngine;
using System.Collections;

public class PlayerCollisions : MonoBehaviour
{
	public Player player;

	[HideInInspector]
	public bool collisionThisFixedFrame = false;

	void FixedUpdate()
	{
		collisionThisFixedFrame = false;
	}

	void OnCollisionStay2D(Collision2D collision)
	{
	}
		
	void OnTriggerEnter2D(Collider2D other)
	{
        GameObject obj = other.gameObject;
		Rigidbody2D rb = obj.GetComponentInParent<Rigidbody2D>();

        if (rb)
            obj = rb.gameObject;

        if (obj.CompareTag("Spawner"))
		{
			Spawner newSpawner = obj.GetComponent<Spawner>();
			if (newSpawner)
				player.respawner.SetCurrentSpawner(newSpawner);
		}

		if (obj.CompareTag("ShapeItem"))
		{
			ShapeItem shapeItem = obj.GetComponent<ShapeItem>();
			if (shapeItem)
				shapeItem.OnPickUp(player);
		}

		if (obj.CompareTag("DestroyableBlock"))
		{
			DestroyableObject destroyableBlock = obj.GetComponent<DestroyableObject>();
			if (destroyableBlock)
				destroyableBlock.Destroy();
		}

        if (obj.CompareTag("ActionItem"))
        {
            ActionItem actionItem = obj.GetComponent<ActionItem>();

            if (actionItem)
                actionItem.OnAction(player);
        }
    }
}
