﻿using UnityEngine;
using System.Collections;

public class Respawner : MonoBehaviour
{
	public GameObject healPrefab;

	public delegate void VoidDelegate();

	public bool saveEvenIfAlreadySelected = true;
	public bool resetShapeCurrentOnRespawn = true;

	[HideInInspector]
	public Spawner currentSpawner;
	[HideInInspector]
	public Player player;
	[HideInInspector]
	public VoidDelegate onRespawn;

	void Start ()
	{
		player = GetComponent<Player>();
	}

	public void Respawn()
	{
		player.transform.position = currentSpawner.spawnPoint.position;
		player.rotate.rotationDir = currentSpawner.rotationDir;
		if (onRespawn != null)
			onRespawn();
	}

	public void OnReachSpawner()
	{
		if (healPrefab && player && player.life < player.baseLife)
		{
			GameObject effect = Instantiate(healPrefab);

			GameObject target;
			//Spawn on player
			target = gameObject;
			//Spawn on spawner
			target = currentSpawner.gameObject;

			effect.transform.position = target.transform.position;
		}
		if (player)
			player.SetLife(player.baseLife);
	}

	public void SetCurrentSpawner(Spawner newSpawner)
	{
		if (currentSpawner == newSpawner)
		{
			if (saveEvenIfAlreadySelected)
				OnReachSpawner();
			return;
		}

		if (currentSpawner)
			currentSpawner.StopUse();

		currentSpawner = newSpawner;
		if (player)
		{
			currentSpawner.Use(player.rotate.rotationDir);
			OnReachSpawner();
		}
		else
			currentSpawner.Use(-1);
	}
}
