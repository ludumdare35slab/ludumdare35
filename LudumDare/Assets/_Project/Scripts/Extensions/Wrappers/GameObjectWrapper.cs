﻿using UnityEngine;
using System;
using System.Collections;

public static class GameObjectWrapper
{
	#region METHODS
	static public T Create<T>(string name, Transform parent = null) where T : Component
	{
		T obj = new GameObject(name).GetComponent<T> () as T;
		if (obj == null)
		{
			Debug.LogError ("GameObjectWrapper - Create: \"" + typeof(T).FullName + "\" instantiation failed!");
			return null;
		}

		if (parent != null)
			obj.transform.SetParent(parent, true);

        LevelInfo levelInfo = GameObject.FindObjectOfType<LevelInfo>();
        if (levelInfo != null)
            levelInfo.AddMeshToSceneContainer(obj.gameObject);
        else
            Debug.LogWarning("GameObjectWrapper - Create: \"LevelInfo\" object not found.");

        return obj;
	}

	static public T Instantiate<T>(string prefabPath, Transform parent = null) where T : Component
	{
		GameObject go = Resources.Load(prefabPath) as GameObject;
		if (go == null)
		{
			Debug.LogError("GameObjectWrapper - Instantiate: prefab loading has failed!");
			return null;
		}
		
		T obj = GameObject.Instantiate (go).GetComponent<T> ();
		if (obj == null)
		{
			Debug.LogError ("GameObjectWrapper - Instantiate: \"" + typeof(T).FullName + "\" instantiation has failed!");
			return null;
		}

		if (parent != null)
			obj.transform.SetParent(parent, false);

        LevelInfo levelInfo = GameObject.FindObjectOfType<LevelInfo>();
        if (levelInfo != null)
            levelInfo.AddMeshToSceneContainer(obj.gameObject);
        else
            Debug.LogWarning("GameObjectWrapper - Instantiate: \"LevelInfo\" object not found.");

        return obj;
	}
	
	static public T Instantiate<T>(GameObject prefab, Transform parent = null) where T : Component
	{
		if (prefab == null)
		{
			Debug.LogError("GameObjectWrapper - Instantiate: prefab \"" + prefab + "\" loading has failed!");
			return null;
		}

		T obj = GameObject.Instantiate (prefab).GetComponent<T> ();
		if (obj == null)
		{
			Debug.LogError ("GameObjectWrapper - Instantiate: \"" + typeof(T).FullName + "\" instantiation has failed!");
			return null;
		}

		if (parent != null)
			obj.transform.SetParent(parent, false);

        LevelInfo levelInfo = GameObject.FindObjectOfType<LevelInfo>();
        if (levelInfo != null)
            levelInfo.AddMeshToSceneContainer(obj.gameObject);
        else
            Debug.LogWarning("GameObjectWrapper - Instantiate: \"LevelInfo\" object not found.");

        return obj;
	}

    static public void Destroy(GameObject obj)
    {
        LevelInfo levelInfo = GameObject.FindObjectOfType<LevelInfo>();
        if (levelInfo != null)
            levelInfo.RemoveMeshFromSceneContainer(obj.gameObject);
        else
            Debug.LogWarning("GameObjectWrapper - Instantiate: \"LevelInfo\" object not found.");

        GameObject.Destroy(obj);
    }

    static public void Destroy(GameObject obj, float timer)
    {
        GameObject.Destroy(obj, timer);

        LevelInfo levelInfo = GameObject.FindObjectOfType<LevelInfo>();
        if (levelInfo != null)
            levelInfo.RemoveMeshFromSceneContainer(obj.gameObject);
        else
            Debug.LogWarning("GameObjectWrapper - Instantiate: \"LevelInfo\" object not found.");
    }
    #endregion
}
