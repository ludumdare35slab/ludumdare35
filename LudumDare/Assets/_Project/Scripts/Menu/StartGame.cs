﻿using UnityEngine;
using System.Collections;

public class StartGame : MonoBehaviour
{
    public Animator pressAnyKeyTextAnimator = null;
    public Animator titleTextAnimator = null;
    public float fadeTime = 1f;

	public AudioClip clip;

    bool hasStarted = false;

	void Update ()
    {
        if (hasStarted == false)
        {
            if (Input.anyKeyDown)
            {
                StartCoroutine(Start_Coroutine());
                hasStarted = true;
            }
        }
	}

    IEnumerator Start_Coroutine()
    {
        pressAnyKeyTextAnimator.SetBool("HasStarted", true);
        titleTextAnimator.SetBool("HasStarted", true);

		if (clip)
			SoundManager.Instance.PlaySound2D(clip);

        yield return new WaitForSeconds(fadeTime);

        GameObject anyKeyGo = pressAnyKeyTextAnimator.gameObject;
        anyKeyGo.SetActive(false);

        GameObject titleTextGo = titleTextAnimator.gameObject;
        titleTextGo.SetActive(false);

        Device.Instance.nextState = Device.GameState.Level;

        yield return null;
    }
}
