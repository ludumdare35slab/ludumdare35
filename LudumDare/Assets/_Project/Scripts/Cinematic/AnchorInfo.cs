﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;

public class AnchorInfo : MonoBehaviour
{

	[System.Serializable]
	public class CinematicEvent
	{
		public UnityEvent action = null;
		public float timeFromPrevReached = 0f;
	}
	public List<CinematicEvent> events;
	public float newCameraSpeed = 10f;
	public float pauseWhenReached = 0f;
	public UnityEvent actionWhenReached = null;
}
