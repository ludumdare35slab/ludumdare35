﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System;
using System.Collections;
using System.Collections.Generic;

public class Cinematic : MonoBehaviour
{
    public string soundNameToPlay = string.Empty;
    public float pauseBeforeEnding = 0f;
    public float pauseAfterIntro = 0f;
    public float returnToPlayerSpeed = 10f;
    public List<AnchorInfo> anchors = null;

    [HideInInspector]
    public int targetIndex = 0;
    [HideInInspector]
    public int count = 0;

    Animator inGameCanvasAnimator = null;

    public UnityEvent Play { get; private set; }
    public bool IsPlaying { get; private set; }

    Camera gameCamera = null;

	Follow cameraFollow;
	float cinematicTimer = 0;

	//Events
	class Event
	{
		public float time;
		public UnityEvent ev;
	}
	List<Event> incommingEvents;

	void Awake ()
    {
        if (gameCamera == null)
            gameCamera = Camera.main;

        Play = new UnityEvent();
        Play.AddListener(PlayCinematic);

        inGameCanvasAnimator = Device.Instance.inGameCanvas.GetComponent<Animator>();
    }

	void Start()
	{
		cameraFollow = CinematicManager.Instance.cameraFollow;
		incommingEvents = new List<Event>();
	}

    void PlayCinematic()
    {
        IsPlaying = true;
        StartCoroutine(PlayCinematic_Coroutine());
    }

    // Pause

    // Stop

    IEnumerator PlayCinematic_Coroutine()
	{
		cameraFollow.OnReachedTarget.AddListener(AnchorReached);

		float prevTimeScale = Time.timeScale;
		bool isCameraFollowPrevTimeScaled = cameraFollow.scaledTime;
		// Time off
		Time.timeScale = 0f;
		cameraFollow.scaledTime = false;

		// Controls off
		GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().SetControls(false);

        // GUI off
        inGameCanvasAnimator.SetTrigger("Disappear");

        // Frames on
        StartCoroutine(MoveFrames_Coroutine(false));
        yield return StartCoroutine(Utils.WaitForRealTimeCoroutine(CinematicManager.Instance.introDuration));

        yield return StartCoroutine(Utils.WaitForRealTimeCoroutine(pauseAfterIntro));

		cinematicTimer = 0;

		// Camera Behavior
		Follow follow = CinematicManager.Instance.cameraFollow;
        if (anchors[0] != null)
        {
			NextAnchor();
			while (targetIndex <= anchors.Count)
			{
				UpdateWhilePlayingCinematic();
                yield return null;
            }
        }
        else
            Debug.LogError("CinematicManager: No anchor to snap to. Please add one!");

		incommingEvents.Clear();
		cinematicTimer = 0;

        // Secret sound
		if (soundNameToPlay != "")
		  SoundManager.Instance.PlaySound2D(soundNameToPlay);

        yield return StartCoroutine(Utils.WaitForRealTimeCoroutine(pauseBeforeEnding));

        // Frames off
        StartCoroutine(MoveFrames_Coroutine(true));

        // GUI on
        inGameCanvasAnimator.SetTrigger("Appear");

        // Return on player
        cameraFollow.speed = returnToPlayerSpeed;
        cameraFollow.target = GameObject.FindGameObjectWithTag("Player").transform;

        yield return StartCoroutine(Utils.WaitForRealTimeCoroutine(CinematicManager.Instance.introDuration));

        // Controls on
        GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().SetControls(true);

		//Time on
		Time.timeScale = prevTimeScale;
		cameraFollow.scaledTime = isCameraFollowPrevTimeScaled;

		follow.speed = follow.savedSpeed;

        cameraFollow.OnReachedTarget.RemoveListener(AnchorReached);
    }

    IEnumerator MoveFrames_Coroutine(bool isReversed)
    {
        float elapsedTime = 0f;
        while (elapsedTime < CinematicManager.Instance.introDuration)
        {
            elapsedTime += Time.unscaledDeltaTime;
            if (isReversed == false)
            {
                CinematicManager.Instance.topFrame.rectTransform.anchoredPosition = new Vector2(0, Mathf.Lerp(CinematicManager.Instance.TopFrameStartAnchoredPosition.y,
                                                                                                              0,
                                                                                                              elapsedTime / CinematicManager.Instance.introDuration));
                CinematicManager.Instance.bottomFrame.rectTransform.anchoredPosition = new Vector2(0, Mathf.Lerp(CinematicManager.Instance.BottomFrameStartAnchoredPosition.y,
                                                                                                                 0,
                                                                                                                 elapsedTime / CinematicManager.Instance.introDuration));
            }
            else
            {
                CinematicManager.Instance.topFrame.rectTransform.anchoredPosition = new Vector2(0, Mathf.Lerp(0, CinematicManager.Instance.TopFrameStartAnchoredPosition.y,
                                                                                                              elapsedTime / CinematicManager.Instance.introDuration));
                CinematicManager.Instance.bottomFrame.rectTransform.anchoredPosition = new Vector2(0, Mathf.Lerp(0, CinematicManager.Instance.BottomFrameStartAnchoredPosition.y,
                                                                                                                 elapsedTime / CinematicManager.Instance.introDuration));
            }

            yield return null;
        }
    }

	void UpdateWhilePlayingCinematic()
	{
		cinematicTimer += Time.unscaledDeltaTime;
		while (incommingEvents.Count > 0 && incommingEvents[0].time < cinematicTimer)
		{
			incommingEvents[0].ev.Invoke();
			incommingEvents.RemoveAt(0);
		}
	}
	IEnumerator ReachedTargetCoroutine()
	{
		anchors[targetIndex - 1].actionWhenReached.Invoke();
		yield return StartCoroutine(Utils.WaitForRealTimeCoroutine(anchors[targetIndex - 1].pauseWhenReached));
		NextAnchor();
	}

	void NextAnchor()
	{
		if (targetIndex < anchors.Count)
		{
			AnchorInfo anchor = anchors[targetIndex];

			cameraFollow.target = anchor.transform;
			cameraFollow.speed = anchor.newCameraSpeed;

			foreach (AnchorInfo.CinematicEvent cinematicEvent in anchor.events)
			{
				Event ev = new Event();
				ev.time = cinematicEvent.timeFromPrevReached + cinematicTimer;
				ev.ev = cinematicEvent.action;
				incommingEvents.Add(ev);
			}
		}

		++targetIndex;
	}

	public void AnchorReached()
	{
		StartCoroutine(ReachedTargetCoroutine());
	}
}
