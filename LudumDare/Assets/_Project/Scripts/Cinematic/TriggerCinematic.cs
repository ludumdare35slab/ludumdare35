﻿using UnityEngine;
using System.Collections;

public class TriggerCinematic : MonoBehaviour
{
    public string cinematicName = string.Empty;

	public Color gizmosColor;
	public BoxCollider2D collision;

    void OnTriggerEnter2D(Collider2D col)
    {
        GameObject obj = col.gameObject.GetComponentInParent<Rigidbody2D>().gameObject;
        if (obj.CompareTag("Player"))
        {
            CinematicManager.Instance.LaunchCinematic(cinematicName);
            gameObject.SetActive(false);
        }
    }

	void OnDrawGizmos()
	{
		Gizmos.color = gizmosColor;
		Gizmos.DrawCube(transform.position, new Vector3 (collision.size.x, collision.size.y, 1));
	}
}
