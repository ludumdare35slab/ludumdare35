﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CustomTerrain2D : MonoBehaviour
{
	public List<Vector3> points;

	public float maxStreched;
	public float verticalSize;
	public Vector2 snap;
	public bool loop;
    public bool insert = false;
	public Section section;
	[Range(0, 100)]
	public float preferedTileLength = 1;
    public float diskSize = 2;

    Mesh generatedMesh;
	public Mesh GeneratedMesh
	{
		set
		{
			if (generatedMesh != value)
			{
				if (section.filter.sharedMesh != generatedMesh)
					DestroyImmediate(section.filter.sharedMesh);
				DestroyImmediate(generatedMesh);
				generatedMesh = value;
				section.filter.mesh = value;
			}
		}
	}
}
