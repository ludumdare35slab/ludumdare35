﻿using UnityEngine;
using System.Collections;

public class ShieldSpawner : MonoBehaviour
{
	public GameObject shield;
	Player player;

	void Start ()
	{
		LevelController controller = GameObject.FindObjectOfType<LevelController>();
		if (!controller.PlayerSpawned)
			controller.OnPlayerSpawn.AddListener(GetPlayer);
		else
			GetPlayer();
		ResetShieldPosition();
	}

	void GetPlayer()
	{
		player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
		player.respawner.onRespawn += ResetShieldPosition;
	}
	
	void ResetShieldPosition()
	{
		shield.transform.localPosition = Vector3.zero;
		shield.transform.localRotation = Quaternion.identity;
	}
}
