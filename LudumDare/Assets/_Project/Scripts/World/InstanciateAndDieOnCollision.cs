﻿using UnityEngine;
using System.Collections;

public class InstanciateAndDieOnCollision : MonoBehaviour
{
	public GameObject prefab = null;

	void OnCollisionStay2D(Collision2D collision)
	{
		if (prefab)
		{
			GameObject obj = Instantiate(prefab);
			obj.transform.position = transform.position;
			obj.transform.rotation = transform.rotation;
		}
		Destroy(gameObject);
	}
}
