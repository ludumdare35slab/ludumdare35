﻿using UnityEngine;
using System.Collections;
using System;

public class RotationReverser : ActionItem
{
	AudioSource audioSource;

	public SpriteRenderer spriteRenderer;
	public Sprite spriteCorner;
	public Sprite spriteNormal;
	public bool isCorner;

	void Start()
	{
		audioSource = GetComponent<AudioSource>();

		if (isCorner)
			spriteRenderer.sprite = spriteCorner;
		else 
			spriteRenderer.sprite = spriteNormal;
	}

	public override void OnAction(Player player)
	{
		player.rotate.rotationDir *= -1;
		audioSource.Play();
	}
}
