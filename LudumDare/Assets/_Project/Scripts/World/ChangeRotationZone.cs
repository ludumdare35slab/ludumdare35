﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections.Generic;

public class ChangeRotationZone : MonoBehaviour
{
	public UnityEvent OnPlayerEnter;
	public UnityEvent OnPlayerExit;

	public Sprite InSprite;
	public Sprite OutSprite;

	List<Collider2D> collidersIn = new List<Collider2D>();
	SpriteRenderer spriteRenderer;

	void Start()
	{
		spriteRenderer = GetComponent<SpriteRenderer>();
		OnPlayerEnter.AddListener(() => spriteRenderer.sprite = InSprite);
		OnPlayerExit.AddListener(() => spriteRenderer.sprite = OutSprite);
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (!other.attachedRigidbody.CompareTag("Player"))
			return;

		Player player = other.attachedRigidbody.GetComponent<Player>();
		if (player)
		{
			if (collidersIn.Count == 0)
			{
				ChangeRotation(player);
				OnPlayerEnter.Invoke();	
			}

			collidersIn.Add(other);
		}
	}

	void OnTriggerExit2D(Collider2D other)
	{
		if (!other.attachedRigidbody.CompareTag("Player"))
			return;

		collidersIn.Remove(other);
		if (collidersIn.Count == 0)
			OnPlayerExit.Invoke();
	}

	void ChangeRotation(Player player)
	{
		player.rotate.rotationDir *= -1;
	}
}
