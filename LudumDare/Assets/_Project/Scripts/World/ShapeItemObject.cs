﻿using UnityEngine;
using System.Collections;

public class ShapeItemObject : ScriptableObject
{
	public string shapeName;
	public GameObject prefab;
	public GameObject previewPrefab;
    public Sprite iconNotSelected;
    public Sprite iconSelected;
    public Sprite worldSprite;
	public bool canDestroyBlock;
}
