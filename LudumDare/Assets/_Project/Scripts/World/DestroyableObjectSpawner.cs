﻿using UnityEngine;
using System.Collections;

public class DestroyableObjectSpawner : MonoBehaviour
{
	public Player player;
	public GameObject prefab;
	public float respawnTime = 3f;
	public float minDistanceFromPlayer = 5f;

	[HideInInspector]
	public float timer = 0f;
	public DestroyableObject obj;
	void Start()
	{
		LevelController controller = GameObject.FindObjectOfType<LevelController>();
		if (!controller.PlayerSpawned)
			controller.OnPlayerSpawn.AddListener(GetPlayer);
		else
			GetPlayer();
		obj.onDestroy += BlockDestroyed;
	}

	void GetPlayer()
	{
		player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
		player.respawner.onRespawn += RespawnBlock;
	}

	void RespawnBlock()
	{
		if(obj == null)
		{
			GameObject newBlock = Instantiate(prefab);
			newBlock.transform.SetParent(transform, false);
			obj = newBlock.GetComponent<DestroyableObject>();

			obj.onDestroy += BlockDestroyed;
			isTimerNeeded = false;
			timer = 0;
		}
	}

	bool isTimerNeeded = false;

	void Update()
	{
		if (obj == null)
		{
			timer -= Time.deltaTime;
			if (timer <= 0f && (gameObject.transform.position - player.transform.position).magnitude > minDistanceFromPlayer && isTimerNeeded)
				RespawnBlock();
		}
	}

	void BlockDestroyed()
	{
		timer = respawnTime;
		obj = null;
		isTimerNeeded = true;
	}
}
