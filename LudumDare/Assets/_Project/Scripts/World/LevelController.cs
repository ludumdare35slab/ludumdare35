﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public class LevelController : MonoBehaviour
{
	public Spawner levelFirstSpawner;
	public GameObject playerPrefab;

	[HideInInspector]
	public Player playerCpnt;

	public bool PlayerSpawned { get; private set; }
	public UnityEvent OnPlayerSpawn = new UnityEvent();

	void Start()
	{
		Device.Instance.OnStateChanged.AddListener(StateChanged);
	}

	void SpawnPlayer()
	{
		GameObject player = Instantiate(playerPrefab);
		player.transform.position = levelFirstSpawner.spawnPoint.position;
		playerCpnt = player.GetComponent<Player>();
		playerCpnt.respawner.SetCurrentSpawner(levelFirstSpawner);
		PlayerSpawned = true;
		OnPlayerSpawn.Invoke();
	}

	void StateChanged(Device.GameState currentState, Device.GameState previousState)
	{
		switch (currentState)
		{
			case Device.GameState.None:
				break;
			case Device.GameState.Intro:
				break;
			case Device.GameState.Menu:
				break;
			case Device.GameState.Level:
				if (previousState == Device.GameState.Intro)
					SpawnPlayer();
				break;
			default:
				break;
		}
	}
}
