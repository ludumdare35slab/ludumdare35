﻿using UnityEngine;
using System.Collections;

public class Magnet : MonoBehaviour
{
	public Transform shieldOrigin;

	public float attractionPower;
	public float rotationnalPower;

	GameObject shield;
	Player player;
	void Start()
	{
		LevelController controller = GameObject.FindObjectOfType<LevelController>();
		if (!controller.PlayerSpawned)
			controller.OnPlayerSpawn.AddListener(GetPlayer);
		else
			GetPlayer();
	}

	void GetPlayer()
	{
		player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
		player.respawner.onRespawn += ReleaseShield;
		player.onShapeSwap += ShapeSwap;
	}

	void ShapeSwap(ShapeItemObject newItem)
	{
		shield = null;
	}

	void ReleaseShield()
	{
		shield = null;
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (!shield)
			shield = other.gameObject;
	}

	void LateUpdate()
	{
		if (shield)
		{
			Vector3 targetPos = shieldOrigin.position;
			Vector3 thisToTarget = targetPos - shield.transform.position;
			Vector3 direction = Vector3.Normalize(thisToTarget);

			if (thisToTarget.magnitude < attractionPower * Time.deltaTime)
				shield.transform.position = targetPos;
			else
				shield.transform.position += direction * attractionPower * Time.deltaTime;

			float rotationNeeded = Quaternion.Angle(shield.transform.rotation, shieldOrigin.transform.rotation);
			float rotationAngle = rotationnalPower * Time.deltaTime;

			if (rotationNeeded> rotationAngle)
				shield.transform.rotation = shieldOrigin.transform.rotation;
			else
				shield.transform.Rotate(0, 0, Mathf.Sign(Mathf.DeltaAngle(rotationAngle, rotationNeeded)) * rotationAngle);
		}
	}
}
