﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Shooter : MonoBehaviour
{
	public GameObject missilePrefab;
	public Transform missileSpawner;
	public AudioSource source;

	public float awakeTime;
	public List<float> waitTimes;

	void Start ()
	{
		StartCoroutine(PlayMissileLaunchCoroutine());
	}

	void SpawnMissile()
	{
		GameObject missile = Instantiate(missilePrefab);
		missile.transform.SetParent(missileSpawner, false);
		source.Play();
	}

	IEnumerator PlayMissileLaunchCoroutine()
	{
		yield return new WaitForSeconds(awakeTime);

		int count = waitTimes.Count;
		for (int i = 0; true; i = (i + 1) % count)
		{
			SpawnMissile();
			yield return new WaitForSeconds(waitTimes[i]);
		}
	}
}
