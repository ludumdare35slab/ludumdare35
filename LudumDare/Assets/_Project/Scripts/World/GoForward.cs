﻿using UnityEngine;
using System.Collections;

public class GoForward : MonoBehaviour
{
	public float speed = 1f;

	void Update ()
	{
		transform.position += transform.right * speed * Time.deltaTime;
	}
}
