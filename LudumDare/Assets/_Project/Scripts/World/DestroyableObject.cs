﻿using UnityEngine;
using System.Collections;

public class DestroyableObject : MonoBehaviour
{
	public delegate void OnDestroy();
	public OnDestroy onDestroy;

	public void Destroy()
	{
		if (onDestroy != null)
			onDestroy();
		GameObject.Destroy(gameObject);
	}
}
