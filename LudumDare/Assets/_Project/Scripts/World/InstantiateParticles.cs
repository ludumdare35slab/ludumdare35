﻿using UnityEngine;
using System.Collections;

public class InstantiateParticles : MonoBehaviour
{
	public GameObject particlesPrefabs;
	public Transform target;
	public bool onDestroy;


	bool _isQuitting = false;

	void OnApplicationQuit()
	{
		_isQuitting = true;
	}


	public void Instantiate()
	{
		if (particlesPrefabs)
			Instantiate(particlesPrefabs, target.position, target.rotation);
	}

	void OnDestroy()
	{
		if (onDestroy && !Application.isLoadingLevel && !_isQuitting)
			Instantiate();
	}
}
