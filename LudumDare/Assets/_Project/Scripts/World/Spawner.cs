﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour
{
	public Transform spawnPoint;
	public SpriteRenderer sprite;
    public Sprite selected;
    public Sprite unSelected;

	public AudioClip activateSound;
	public AudioClip deactivateSound;

	[HideInInspector]
	public float rotationDir;

	public void Use(float newRotationDir)
	{
		sprite.sprite = selected;
		rotationDir = newRotationDir;
		if (activateSound)
			SoundManager.Instance.PlaySound2D(activateSound);
	}

	public void StopUse()
	{
		sprite.sprite = unSelected;
		if (deactivateSound)
			SoundManager.Instance.PlaySound2D(deactivateSound);
	}

	void Start()
	{
	}
}
