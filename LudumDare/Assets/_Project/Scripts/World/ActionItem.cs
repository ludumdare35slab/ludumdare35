﻿using UnityEngine;
using System.Collections;

public abstract class ActionItem : MonoBehaviour
{
	public abstract void OnAction(Player player);
}
