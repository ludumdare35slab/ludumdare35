﻿using UnityEngine;
using System.Collections;
using System;

public class ShapeItem : Item
{
	public ShapeItemObject shapeScriptableObject;

	public AudioClip pickUpSound;
	public SpriteRenderer spriteRenderer;

	void Start()
	{
		spriteRenderer.sprite = shapeScriptableObject.worldSprite;
	}

	public override void OnPickUp(Player player)
	{
		player.AddShape(shapeScriptableObject);
		SoundManager.Instance.PlaySound2D(pickUpSound);
		Destroy(gameObject);
	}
}
