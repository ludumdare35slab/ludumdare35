﻿using UnityEngine;
using System.Collections;

public class Zoom : MonoBehaviour
{
	[HideInInspector]
	public float orthoSizeTarget;
	[HideInInspector]
	public float zoomSpeed;

	Camera cam;

	void Start()
	{
		cam = Camera.main;
	}
	public void ChangeZoomTarget(float newOrthoZoom, float newZoomSpeed)
	{
		orthoSizeTarget = newOrthoZoom;
		zoomSpeed = newZoomSpeed;
	}

	void Update()
	{
		float move = Mathf.Sign(orthoSizeTarget - cam.orthographicSize) * zoomSpeed * Time.deltaTime;

		if (Mathf.Abs(orthoSizeTarget - cam.orthographicSize) < zoomSpeed * Time.deltaTime)
			cam.orthographicSize = orthoSizeTarget;
		else
			cam.orthographicSize += move;
	}
}
