﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;

public class TriggerShake : MonoBehaviour
{
    public bool isRandom = true;
    public Vector3 direction = Vector3.one;
    public float shakeAmount = 10f;
    public float shakeTime = 1f;

	public Color gizmosColor;
	public BoxCollider2D collision;

    void OnTriggerEnter2D(Collider2D col)
    {
        GameObject obj = col.gameObject.GetComponentInParent<Rigidbody2D>().gameObject;
        if (obj.CompareTag("Player"))
        {
            if (isRandom)
                FxManager.Instance.RShakeCamera(shakeTime, shakeAmount);
            else
                FxManager.Instance.VShakeCamera(direction, shakeTime, shakeAmount);
        }
    }

	void OnDrawGizmos()
	{
		Gizmos.color = gizmosColor;
		Gizmos.DrawCube(transform.position, new Vector3 (collision.size.x, collision.size.y, 1));
	}

}
