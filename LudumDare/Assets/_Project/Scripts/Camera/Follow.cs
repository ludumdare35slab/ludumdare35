﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public class Follow : MonoBehaviour
{
	public Transform target;
	public Vector3 offset;
	public float speed;
	public bool scaledTime = true;

    [HideInInspector]
    public UnityEvent OnReachedTarget = null;
    [HideInInspector]
    public bool hasReachedTarget = false;
    [HideInInspector]
    public float savedSpeed = 0f;

    bool hasUpdatedReachedTarget = false;

    void Awake()
    {
        OnReachedTarget = new UnityEvent();
        savedSpeed = speed;
    }

	void Start()
	{
		LevelController controller = GameObject.FindObjectOfType<LevelController>();
		if (!controller.PlayerSpawned)
			controller.OnPlayerSpawn.AddListener(GetPlayer);
		else
			GetPlayer();
	}

	void GetPlayer()
	{
		if (target == null)
			target = GameObject.FindGameObjectWithTag("Player").transform;
	}

	void LateUpdate ()
	{
		if (!target)
			return;
			
		Vector3 targetPos = target.position + offset;
		Vector3 thisToTarget = targetPos - transform.position;
		Vector3 direction = Vector3.Normalize(thisToTarget);

		float moveDistance;
		if (scaledTime)
			moveDistance = speed * Time.deltaTime;
		else
			moveDistance = speed * Time.unscaledDeltaTime;

		if (thisToTarget.magnitude < moveDistance)
        {
			transform.position = targetPos;
            if (hasUpdatedReachedTarget == false)
            {
                if (OnReachedTarget != null)
                    OnReachedTarget.Invoke();

                hasUpdatedReachedTarget = true;
                hasReachedTarget = true;
            }
        }
		else
        {
			transform.position += direction * moveDistance;

            hasUpdatedReachedTarget = false;
            hasReachedTarget = false;
        }
	}
}
