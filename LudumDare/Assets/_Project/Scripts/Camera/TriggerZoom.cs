﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;

public class TriggerZoom : MonoBehaviour
{
    public float newOrthoSize = 10f;
	public float zoomSpeed = 3f;

	public Color gizmosColor;
	public BoxCollider2D collision;

	Zoom zoom;

	void Start()
	{
		zoom = Camera.main.GetComponent<Zoom>();
	}

	void OnTriggerEnter2D(Collider2D col)
    {
        GameObject obj = col.gameObject.GetComponentInParent<Rigidbody2D>().gameObject;
        if (obj.CompareTag("Player"))
			zoom.ChangeZoomTarget(newOrthoSize, zoomSpeed);
    }

	void OnDrawGizmos()
	{
		Gizmos.color = gizmosColor;
		Gizmos.DrawCube(transform.position, new Vector3 (collision.size.x, collision.size.y, 1));
	}
}
