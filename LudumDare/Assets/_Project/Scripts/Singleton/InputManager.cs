﻿using UnityEngine;
using System.Collections;

public class GameLayer
{
	public delegate void AxisDelegate(float axis);
	public delegate void VoidDelegate();
	public delegate void StringDelegate(string name);

	public StringDelegate LineDown;
	public StringDelegate HammerDown;
	public StringDelegate MagnetDown;
	public StringDelegate TriangleDown;
	public StringDelegate LineUp;
	public StringDelegate HammerUp;
	public StringDelegate MagnetUp;
	public StringDelegate TriangleUp;
	
	public VoidDelegate PauseUp;
	public VoidDelegate PauseDown;
	
	public AxisDelegate Horizontal;
	public AxisDelegate Vertical;
}

public class MenuLayer
{
	public delegate void VoidDelegate();

	public VoidDelegate PauseUp;
	public VoidDelegate PauseDown;
}

public class InputManager
	: MonoSingleton<InputManager>
{
	public enum State
	{
		Menu,
		Game
	}

	public enum DeviceType
	{
		Controller,
		Keyboard
	}

	public delegate void DeviceTypeDelegate(DeviceType type);

	public DeviceTypeDelegate deviceChanged;
	public DeviceType currentDeviceType;

	public MenuLayer Menu { get; private set; }
	public GameLayer Game { get; private set; }

	public State CurrentState { get; set; }

	void Awake()
	{
		Menu = new MenuLayer();
		Game = new GameLayer();
	}

	void Start ()
	{
		currentDeviceType = DeviceType.Keyboard;
		CurrentState = State.Menu;
	}

	public string GetTriangleButton()
	{
		if (currentDeviceType == DeviceType.Controller)
			return "X";
		else
			return "Q";
	}
	public string GetLineButton()
	{
		if (currentDeviceType == DeviceType.Controller)
			return "Y";
		else
			return "Z";
	}
	public string GetMagnetButton()
	{
		if (currentDeviceType == DeviceType.Controller)
			return "A";
		else
			return "S";
	}
	public string GetHammerButton()
	{
		if (currentDeviceType == DeviceType.Controller)
			return "B";
		else
			return "D";
	}

	void Update()
	{
		DeviceType newDeviceType = TestCurrentDeviceType();
		if (newDeviceType != currentDeviceType)
		{
			currentDeviceType = newDeviceType;
			if (deviceChanged != null)
				deviceChanged(newDeviceType);
		}

		switch (CurrentState)
		{
			case State.Menu:
				if (Input.GetButtonDown("Pause") && Menu.PauseDown != null)
					Menu.PauseDown();
				if (Input.GetButtonUp("Pause") && Menu.PauseUp != null)
					Menu.PauseUp();
				break;
			case State.Game:
				if (Input.GetButtonDown("Line") && Game.LineDown != null)
					Game.LineDown("Line");
				if (Input.GetButtonUp("Line") && Game.LineUp != null)
					Game.LineUp("Line");
				if (Input.GetButtonDown("Hammer") && Game.HammerDown != null)
					Game.HammerDown("Hammer");
				if (Input.GetButtonUp("Hammer") && Game.HammerUp != null)
					Game.HammerUp("Hammer");
				if (Input.GetButtonDown("Magnet") && Game.MagnetDown != null)
					Game.MagnetDown("Magnet");
				if (Input.GetButtonUp("Magnet") && Game.MagnetUp != null)
					Game.MagnetUp("Magnet");
				if (Input.GetButtonDown("Triangle") && Game.TriangleDown != null)
					Game.TriangleDown("Triangle");
				if (Input.GetButtonUp("Triangle") && Game.TriangleUp != null)
					Game.TriangleUp("Triangle");
				if (Input.GetButtonDown("Pause") && Game.PauseDown != null)
					Game.PauseDown();
				if (Input.GetButtonUp("Pause") && Game.PauseUp != null)
					Game.PauseUp();
				if (Game.Horizontal != null)
					Game.Horizontal(Input.GetAxis("Horizontal"));
				if (Game.Vertical != null)
					Game.Vertical(Input.GetAxis("Vertical"));
				break;
			default:
				break;
		}
	}

	DeviceType TestCurrentDeviceType()
	{
		if (Input.GetAxis("HorizontalLeftStick") != 0.0f ||
				Input.GetAxis("VerticalLeftStick") != 0.0f)
		{
			return DeviceType.Controller;
		}

		if (Input.anyKey)
		{
			if (	Input.GetKey(KeyCode.Joystick1Button0) ||
				   Input.GetKey(KeyCode.Joystick1Button1) ||
				   Input.GetKey(KeyCode.Joystick1Button2) ||
				   Input.GetKey(KeyCode.Joystick1Button3) ||
				   Input.GetKey(KeyCode.Joystick1Button4) ||
				   Input.GetKey(KeyCode.Joystick1Button5) ||
				   Input.GetKey(KeyCode.Joystick1Button6) ||
				   Input.GetKey(KeyCode.Joystick1Button7) ||
				   Input.GetKey(KeyCode.Joystick1Button8) ||
				   Input.GetKey(KeyCode.Joystick1Button9) ||
				   Input.GetKey(KeyCode.Joystick1Button10) ||
				   Input.GetKey(KeyCode.Joystick1Button11) ||
				   Input.GetKey(KeyCode.Joystick1Button12) ||
				   Input.GetKey(KeyCode.Joystick1Button13) ||
				   Input.GetKey(KeyCode.Joystick1Button14) ||
				   Input.GetKey(KeyCode.Joystick1Button15) ||
				   Input.GetKey(KeyCode.Joystick1Button16) ||
				   Input.GetKey(KeyCode.Joystick1Button17) ||
				   Input.GetKey(KeyCode.Joystick1Button18) ||
				   Input.GetKey(KeyCode.Joystick1Button19))
			{
				return DeviceType.Controller;
			}
			return DeviceType.Keyboard;
		}

		return currentDeviceType;
	}
}
