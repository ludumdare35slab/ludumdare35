﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public class Device : MonoSingleton<Device>
{
    public enum GameState : byte
    {
        None = 0,
        Intro,
        Menu,
        Level
    }
    [HideInInspector]
    public GameState state;
    [HideInInspector]
    public GameState nextState;

	public class StateEvent : UnityEvent<GameState, GameState>
	{ }
	public StateEvent OnStateChanged = new StateEvent();

	public string sceneToLoad = "Game";

    public Canvas introCanvas = null;
    public Canvas menuCanvas = null;
    public Canvas inGameCanvas = null;
    public Canvas cinematicCanvas = null;

    public bool activateControlsAtStart = false;

    void Awake()
	{
        GameObject.DontDestroyOnLoad(this);
		if (GameObject.FindGameObjectsWithTag("Device").Length > 1)
			Destroy(gameObject);

        state = GameState.None;
        nextState = GameState.Intro;
	}

    void Start()
    {
        if (activateControlsAtStart)
        {
            menuCanvas.enabled = false;
            inGameCanvas.gameObject.SetActive(activateControlsAtStart);
            nextState = GameState.Intro;
        }
    }

    void Update()
    {
        if (nextState != GameState.None)
            SwitchState(nextState);
    }

    void SwitchState(GameState status)
    {
		switch (status)
        {
            case GameState.Intro:
				InputManager.Instance.CurrentState = InputManager.State.Menu;
                break;
            case GameState.Menu:
				InputManager.Instance.CurrentState = InputManager.State.Menu;
                StartCoroutine(LoadLevelAsync_Coroutine(sceneToLoad));
				inGameCanvas.gameObject.SetActive(false);
				menuCanvas.gameObject.SetActive(true);
                break;
			case GameState.Level:
				InputManager.Instance.CurrentState = InputManager.State.Game;
                inGameCanvas.gameObject.SetActive(true);
				menuCanvas.gameObject.SetActive(false);
                break;
			default:
                Debug.LogError("GameManager: Game State has an undefined value (\"" + state + "\"). Please check it up!");
                break;
        }
		var prevState = state;
		state = status;
        nextState = GameState.None;
		OnStateChanged.Invoke(status, prevState);
    }

    IEnumerator LoadLevelAsync_Coroutine(string levelName)
    {
        if (Application.loadedLevelName != levelName)
        {
            AsyncOperation ao = Application.LoadLevelAsync(levelName);
            while (ao.isDone == false)
            {
                yield return null;
            }
        }
    }

	public void QuitGame()
	{
		Application.Quit();
	}
}
