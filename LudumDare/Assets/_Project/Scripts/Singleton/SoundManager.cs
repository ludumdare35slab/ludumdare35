﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(AudioSource))]
public sealed class SoundManager : MonoSingleton<SoundManager>
{
	#region 
	public float MusicVolume { get { return _musicVolume; } set { _musicVolume = value; UpdateMusicVolume(value); } }
	public float SfxVolume { get { return _sfxVolume; } set { _sfxVolume = value; } }

	[SerializeField][Range(0f, 1f)]
	private float _musicVolume = 1f;
    [SerializeField][Range(0f, 1f)] 
	private float _sfxVolume = 1f;
	[SerializeField]
	private string fileName = "_null_";

    private AudioSource MusicSource = null;
	#endregion

	#region EVENTS
	void Awake ()
	{
		if (name != "_null_")
		{
			MusicSource = GetComponent<AudioSource>();
			MusicSource.clip = Resources.Load("Audio/Musics/" + fileName) as AudioClip;
		}
	}
	
	void Start ()
	{
		if (name != "_null_")
		{
			MusicSource.volume = MusicVolume;
			MusicSource.Play();
			MusicSource.loop = true;
		}
	}
	#endregion

	#region METHODS
    public void PlayMusic(string name, bool loop)
    {
        if(name != "_null_")
        {
            MusicSource.clip = Resources.Load("Audio/Musics/" + name) as AudioClip;
            if(loop)
                MusicSource.loop = true;
			MusicSource.volume = MusicVolume;
            MusicSource.Play();
        }
    }

	public void StopMusic(string name)
	{
		if(name != "_null_")
		{
			MusicSource.clip = Resources.Load("Audio/Musics/" + name) as AudioClip;
			MusicSource.Stop();
		}
	}

	public void PauseMusic(string name, bool isPaused)
	{
		if(name != "_null_")
		{
			MusicSource.clip = Resources.Load("Audio/Musics/" + name) as AudioClip;
			if (isPaused)
				MusicSource.Pause();
			else
				MusicSource.UnPause();
		}
	}

	void UpdateMusicVolume(float newVolume)
	{
		MusicSource.volume = newVolume;
	}

    public AudioSource PlaySound2D(string name, float volume = 1f, ulong delay = 0u)
    {
        return PlaySound(name, null, Vector3.zero, volume, true, delay);
    }
	public AudioSource PlaySound2D(AudioClip clip, float volume = 1f, ulong delay = 0u)
	{
		return PlaySound(clip, null, Vector3.zero, volume, true, delay);
	}

	public AudioSource PlaySound(string name, GameObject parentGao, Vector3 pos, float volume = 1f, bool is2d = false, ulong delay = 0u)
    {
        AudioClip clip = Resources.Load("Audio/SFX/" + name) as AudioClip;
        if (clip != null)
		    return PlaySound(clip, parentGao, pos, volume, is2d, delay);
        else
        {
            Debug.LogError("SoundManager - PlaySound: clip not well loaded. Please check the name up!");
            return null;
        }
    }

	public AudioSource PlaySound(AudioClip clip, GameObject parentGao, Vector3 pos, float volume = 1f, bool is2d = false, ulong delay = 0u)
	{
		GameObject soundGao = new GameObject("One shot audio");
		if (is2d)
		{
			soundGao.transform.parent = Camera.main.transform;
			soundGao.transform.localPosition = pos;
		}
		else
		{
			if (parentGao != null)
				soundGao.transform.parent = parentGao.transform;
			soundGao.transform.localPosition = pos;
		}

		AudioSource source = (AudioSource)soundGao.AddComponent<AudioSource>();
		source.clip = clip;
		source.volume = volume * _sfxVolume;
		source.spatialBlend = is2d ? 0f : 1f;
		source.Play(delay);
		Destroy(soundGao, clip.length);

		return source;
	}
	#endregion
}
