﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;

public class CinematicManager : MonoSingleton<CinematicManager>
{
    public float introDuration = 0f;
    public Follow cameraFollow = null;
    public Image topFrame = null;
    public Image bottomFrame = null;

    public Vector2 TopFrameStartAnchoredPosition { get; private set; }
    public Vector2 BottomFrameStartAnchoredPosition { get; private set; }

    [HideInInspector]
    public Dictionary<string, Cinematic> cinematics = null;

    void Start ()
    {
        TopFrameStartAnchoredPosition = topFrame.rectTransform.anchoredPosition;
        BottomFrameStartAnchoredPosition = bottomFrame.rectTransform.anchoredPosition;

        cinematics = new Dictionary<string, Cinematic>();
        GameObject[] cinematicObjects = GameObject.FindGameObjectsWithTag("Cinematic");
        for (int i = 0; i < cinematicObjects.Length; ++i)
        {
            cinematics.Add(cinematicObjects[i].name, cinematicObjects[i].GetComponent<Cinematic>());
        }
	}

    public void LaunchCinematic(string name)
    {
        if (cinematics[name] == null)
        {
            Debug.LogError("Cinematic \"" + name + "\" not properly set. Please check it up!");
            return;
        }

        cinematics[name].Play.Invoke();
    }

    public void LaunchCinematic(Cinematic cinematic)
    {
        if (cinematic == null)
        {
            Debug.LogError("Cinematic not properly set. Please check it up!");
            return;
        }

        cinematic.Play.Invoke();
    }

    public void StopCinematic(string name)
    {
        //if (cinematics[name] == null)
        //{
        //    Debug.LogError("Cinematic \"" + name + "\" not properly set. Please check it up!");
        //    return;
        //}

        //cinematics[name].Stop();
    }

    public void StopCinematic(Cinematic cinematic)
    {
        //if (cinematic == null)
        //{
        //    Debug.LogError("Cinematic not properly set. Please check it up!");
        //    return;
        //}

        //cinematic.Stop();
    }

    void Update ()
    {
	}
}
