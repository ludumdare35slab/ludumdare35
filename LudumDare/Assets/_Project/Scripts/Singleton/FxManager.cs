﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;

/* Class presentation
 * 
 * 
 * 
 * 
 * 
 * 
 */

/* Camera Effects */
public sealed partial class FxManager : MonoSingleton<FxManager> 
{
    #region FIELDS
    [HideInInspector]
    public bool hasCoroutineStarted = false;

    [HideInInspector]
    public Coroutine currentCoroutine = null;
	#endregion

	#region PROPERTIES
	#endregion

    void Awake ()
    {
        GameObject.DontDestroyOnLoad(this);
    }

	#region METHODS
	/// <summary>
	/// Makes the main camera shake in random directions.
	/// </summary>
	public void RShakeCamera(float shakeTime, float shakeAmount, float frequency = 60f)
	{
		StartCoroutine (RShakeCamera_Coroutine(shakeTime, shakeAmount, frequency));
	}
	private IEnumerator RShakeCamera_Coroutine(float shakeTime, float shakeAmount, float frequency)
	{
		Camera camera = Camera.main;
		Vector3 lastShake = Vector3.zero;

		while (shakeTime > 0f)
		{
			camera.transform.localPosition -= lastShake;
			lastShake = UnityEngine.Random.insideUnitCircle * shakeAmount;
			camera.transform.localPosition += lastShake;
			shakeTime -= 1f / frequency;

			yield return new WaitForSeconds(1f / frequency);
		}

		camera.transform.localPosition -= lastShake;
	}

	/// <summary>
	/// Makes the main camera shake in a direction specified by a vector.
	/// </summary>
	public void VShakeCamera(Vector3 direction, float shakeTime, float shakeAmount, float frequency = 60f)
	{
		StartCoroutine (VShakeCamera_Coroutine(direction, shakeTime, shakeAmount, frequency));
	}
	private IEnumerator VShakeCamera_Coroutine(Vector3 direction, float shakeTime, float shakeAmount, float frequency)
	{
		Camera camera = Camera.main;
		Vector3 lastShake = Vector3.zero;
		int updatedDir = 1;

		while (shakeTime > 0f)
		{
			camera.transform.localPosition -= lastShake;
			lastShake = direction * shakeAmount * updatedDir;
			camera.transform.localPosition += lastShake;
			shakeTime -= 1f / frequency;
			updatedDir *= -1;
			
			yield return new WaitForSeconds(1f / frequency);
		}
		
		camera.transform.localPosition -= lastShake;
	}

	/// <summary>
	/// Changes the main camera FOV to perform zooms in or out.
	/// </summary>
	public void Zoom(float newFOV, float zoomTime, float frequency = 60f)
	{
		StartCoroutine (Zoom_Coroutine(newFOV, zoomTime, frequency));
	}
	private IEnumerator Zoom_Coroutine(float targetFOV, float zoomTime, float frequency = 60f)
	{
		Camera camera = Camera.main;
		float originalFOV = camera.fieldOfView;
		float normalizedZoomTime = Mathf.Clamp01(zoomTime);

		while (normalizedZoomTime > 0f)
		{
			camera.fieldOfView = Mathf.Lerp(targetFOV, originalFOV, Mathf.Clamp01(normalizedZoomTime));
			normalizedZoomTime -= 1f / frequency;
				
			yield return new WaitForSeconds(1f / frequency);
		}		
	}

    public void Zoom2D(float newFOV, float zoomTime, float frequency = 60f)
    {
        if (hasCoroutineStarted == false)
        {
            currentCoroutine = StartCoroutine(Zoom2D_Coroutine(newFOV, zoomTime, frequency));
        }
        else
        {
            StopCoroutine(currentCoroutine);
            StartCoroutine(Zoom2D_Coroutine(newFOV, zoomTime, frequency));
        }
    }
    private IEnumerator Zoom2D_Coroutine(float targetFOV, float zoomTime, float frequency = 60f)
    {
        hasCoroutineStarted = true;

        Camera camera = Camera.main;
        float originalFOV = camera.orthographicSize;
        float normalizedZoomTime = Mathf.Clamp01(zoomTime);

        while (normalizedZoomTime > 0f)
        {
            camera.orthographicSize = Mathf.Lerp(targetFOV, originalFOV, Mathf.Clamp01(normalizedZoomTime));
            normalizedZoomTime -= 1f / frequency;

            hasCoroutineStarted = false;
            yield return new WaitForSeconds(1f / frequency);

        }
    }

    /// <summary>
    /// Perfoms a dolly zoom, aka "Vertigo Effect". NOT FUNCTIONAL.
    /// </summary>
    public void DollyZoom(Transform target, float dzTime, float frequency = 60f)
	{
		StartCoroutine (DollyZoom_Coroutine(target, dzTime, frequency));
	}
	private IEnumerator DollyZoom_Coroutine(Transform target, float dzTime, float frequency = 60f)
	{
		Camera camera = Camera.main;
		//float originalFOV = camera.fieldOfView;

		// Calculate the frustum height at a given distance from the camera.
		Func<float, float> FrustumHeightAtDistance = delegate(float dist)
		{
			return 2.0f * dist * Mathf.Tan(camera.fieldOfView * 0.5f * Mathf.Deg2Rad);
		};

		// Calculate the FOV needed to get a given frustum height at a given distance.
		Func<float, float, float> FOVForHeightAndDistance = delegate(float height, float dist)
		{
			return 2.0f * Mathf.Atan(height * 0.5f / dist) * Mathf.Rad2Deg;
		};

		float distance = Vector3.Distance(transform.position, target.position);
		float initHeightAtDist = FrustumHeightAtDistance(distance);

		while (dzTime > 0f)
		{
			// Measure the new distance and readjust the FOV accordingly.
			float currDistance = Vector3.Distance(transform.position, target.position);
			camera.fieldOfView = FOVForHeightAndDistance(initHeightAtDist, currDistance);
			transform.Translate(Vector3.forward * frequency);
			dzTime -= 1f / frequency;

			yield return new WaitForSeconds(1f / frequency);
		}
	}
	#endregion
}

/* Transition Effects */
public sealed partial class FxManager : MonoSingleton<FxManager>
{
	#region FIELDS
	#endregion
	
	#region PROPERTIES
	#endregion
	
	#region METHODS
	/// <summary>
	/// Fade the whole screen from transparency to a specified color by a specified duration.
	/// </summary>
	public void FadeIn(GameObject texturedGo, Color fadeColor, float duration, float frequency = 60f)
	{
		StartCoroutine (FadeIn_Coroutine(texturedGo, fadeColor, duration, frequency));
	}
	private IEnumerator FadeIn_Coroutine(GameObject texturedGo, Color fadeColor, float duration, float frequency = 60f)
	{
		if (texturedGo != null)
		{
			DontDestroyOnLoad(texturedGo);

			GUITexture guiTexture = texturedGo.GetComponent<GUITexture>();

			if (guiTexture == null)
				guiTexture = texturedGo.AddComponent<GUITexture>();

			guiTexture.texture = new Texture2D(8, 8);
			if (guiTexture.texture == null)
				Debug.LogError("FadeIn: Texture2D loading failed.");

			guiTexture.pixelInset = new Rect(0f, 0f, Screen.width, Screen.height);
			Color startColor = fadeColor;
			startColor.a = 0f;
			guiTexture.color = startColor;
			guiTexture.gameObject.layer = LayerMask.NameToLayer("UI");

			while (duration >= 0f)
			{
				guiTexture.color = Color.Lerp(guiTexture.color, fadeColor, 1f / duration * Time.deltaTime);
				duration -= 1f / frequency;
					
				yield return new WaitForSeconds(1f / frequency);
			}
		}
		else
			Debug.LogError ("FadeIn: The Game Object passed as argument is NULL.");
	}

	/// <summary>
	/// Fade the whole screen from a specified color to transparency by a specified duration.
	/// </summary>
	public void FadeOut(GameObject texturedGo, Color fadeColor, float duration, float frequency = 60f)
	{
		StartCoroutine (FadeOut_Coroutine(texturedGo, fadeColor, duration, frequency));
	}
	private IEnumerator FadeOut_Coroutine(GameObject texturedGo, Color fadeColor, float duration, float frequency = 60f)
	{
		if (texturedGo != null)
		{
			GUITexture guiTexture = texturedGo.GetComponent<GUITexture>();
			
			if (guiTexture == null)
				Debug.LogError("The game object passed as argument has no GUITexture. Please use the FadeIn method or add it manually.");
			
			guiTexture.pixelInset = new Rect(0f, 0f, Screen.width, Screen.height);
			Color startColor = fadeColor;
			startColor.a = 1f;
			guiTexture.color = startColor;
			guiTexture.gameObject.layer = LayerMask.NameToLayer("UI");

			while (duration >= 0f)
			{
				guiTexture.color = Color.Lerp(guiTexture.color, fadeColor, 1f / duration * Time.deltaTime);
				duration -= 1f / frequency;
				
				yield return new WaitForSeconds(1f / frequency);
			}
			Destroy(texturedGo);
		}
		else
			Debug.LogError ("FadeOut: The Game Object passed as argument is NULL.");
	}

	/// <summary>
	/// Fade the whole screen from a specified image to transparency by a specified duration.
	/// </summary>
//	public void Dissolve(GameObject texturedGo, float duration, Image image = null, float frequency = 60f)
//	{
//		StartCoroutine (Dissolve_Coroutine(texturedGo, duration, image, frequency));
//	}
//	private IEnumerator Dissolve_Coroutine(GameObject texturedGo, float duration, Image image = null, float frequency = 60f)
//	{
//		if (texturedGo != null)
//		{
//			DontDestroyOnLoad(texturedGo);
//			
//			GUITexture guiTexture = texturedGo.GetComponent<GUITexture>();
//			
//			if (guiTexture == null)
//				guiTexture = texturedGo.AddComponent<GUITexture>();
//
//			Application.CaptureScreenshot("DissolveScreenshot");
//
//			guiTexture.texture = new Texture2D(8, 8);
//			if (guiTexture.texture == null)
//				Debug.LogError("FadeIn: Texture2D loading failed.");
//		}
//		else
//			Debug.LogError ("FadeOut: The Game Object \"" + texturedGo.name + "\" is NULL.");
//	}
	#endregion
}

/* GUI Effects */
public sealed partial class FxManager : MonoSingleton<FxManager>
{
	public enum ScreenPos
	{
		Center,
		TopLeftCorner,
		TopRightCorner,
		BotLeftCorner,
		BotRightCorner,
		Top,
		Bot,
		Left,
		Right,
	}
	
	#region Move
	void Move(RectTransform uiObject, Vector3 from, Vector3 destination, float duration)
	{
		StartCoroutine(Move_Coroutine(uiObject, from, destination, duration));
	}
	/*
	void Move(RectTransform uiObject, Vector3 from, ScreenPos destination, float duration)
	{
		uiObject.transform.position = from;
		Vector3 vDestination = ScreenPosToVector(destination) + RectTransdormOffsetFromScreenPos(destination, uiObject);
		StartCoroutine(Move_Coroutine(uiObject, from, vDestination, duration));
	}

	void Move(RectTransform uiObject, ScreenPos from, Vector3 destination, float duration)
	{
		uiObject.transform.position = destination;
		Vector3 vFrom = ScreenPosToVector(from) + RectTransdormOffsetFromScreenPos(from, uiObject);
		StartCoroutine(Move_Coroutine(uiObject, vFrom, destination, duration));
	}

	void Move(RectTransform uiObject, ScreenPos from, ScreenPos destination, float duration)
	{
		Vector3 vFrom = ScreenPosToVector(from) + RectTransdormOffsetFromScreenPos(from, uiObject);
		uiObject.transform.position = vFrom;
		Vector3 vDestination = ScreenPosToVector(destination) + RectTransdormOffsetFromScreenPos(destination, uiObject);
		StartCoroutine(Move_Coroutine(uiObject, vFrom, vDestination, duration));
	}
	*/
	
	void Come(RectTransform uiObject, Vector3 from, float duration)
	{
		StartCoroutine(Move_Coroutine(uiObject, from, uiObject.transform.position, duration));
	}
	
	void Come(RectTransform uiObject, Vector2 from, float duration)
	{
		StartCoroutine(Move_Coroutine(uiObject,	from, uiObject.transform.position, duration));
	}
	
	void Come(RectTransform uiObject, ScreenPos from, float duration)
	{
		Vector3 vFrom = ScreenPosToVector(from) + RectTransdormOffsetFromScreenPos(from, uiObject);
		
		StartCoroutine(Move_Coroutine(uiObject, vFrom, uiObject.transform.position, duration));
	}
	
	
	void Leave(RectTransform uiObject, Vector3 destination, float duration)
	{
		StartCoroutine(Move_Coroutine(uiObject, uiObject.transform.position, destination, duration));
	}
	
	void Leave(RectTransform uiObject, Vector2 destination, float duration)
	{
		StartCoroutine(Move_Coroutine(uiObject, uiObject.transform.position, destination, duration));
	}
	
	void Leave(RectTransform uiObject, ScreenPos destination, float duration)
	{
		Vector3 vDestination = ScreenPosToVector(destination) + RectTransdormOffsetFromScreenPos(destination, uiObject);
		StartCoroutine(Move_Coroutine(uiObject, uiObject.transform.position, vDestination, duration));
	}
	
	Vector3 ScreenPosToVector(ScreenPos pos)
	{
		Vector3 v;
		switch (pos)
		{
		case ScreenPos.Center:
			v = new Vector2(Screen.width / 2f, Screen.height / 2f);
			break;
		case ScreenPos.TopLeftCorner:
			v = new Vector2(0, Screen.height);
			break;
		case ScreenPos.TopRightCorner:
			v = new Vector2(Screen.width, Screen.height);
			break;
		case ScreenPos.BotLeftCorner:
			v = new Vector2(0, 0);
			break;
		case ScreenPos.BotRightCorner:
			v = new Vector2(Screen.width, 0);
			break;
		case ScreenPos.Top:
			v = new Vector2(Screen.width / 2f, Screen.height);
			break;
		case ScreenPos.Bot:
			v = new Vector2(Screen.width / 2f, 0);
			break;
		case ScreenPos.Left:
			v = new Vector2(0, Screen.height / 2f);
			break;
		case ScreenPos.Right:
			v = new Vector2(Screen.width, Screen.height / 2f);
			break;
		default:
			throw new System.NotImplementedException();
		}
		
		return v;
	}
	
	//Used to make object-specific placement
	Vector3 RectTransdormOffsetFromScreenPos(ScreenPos pos, RectTransform rectTransform)
	{
		Vector3 v;
		
		Vector2 localSpacePivot;
		localSpacePivot.x = rectTransform.pivot.x * rectTransform.rect.width;
		localSpacePivot.y = rectTransform.pivot.y * rectTransform.rect.height;
		
		Rect rect = rectTransform.rect;
		
		switch (pos)
		{
		case ScreenPos.Center:
			v = new Vector2(0, 0);
			break;
		case ScreenPos.TopLeftCorner:
			v = new Vector2(-rect.width - 1, 1);
			v += (Vector3)localSpacePivot;
			break;
		case ScreenPos.TopRightCorner:
			v = new Vector2(1, 1);
			v += (Vector3)localSpacePivot;
			break;
		case ScreenPos.BotLeftCorner:
			v = new Vector2(-rect.width - 1, -rect.height - 1);
			v += (Vector3)localSpacePivot;
			break;
		case ScreenPos.BotRightCorner:
			v = new Vector2(1, -rect.height - 1);
			v += (Vector3)localSpacePivot;
			break;
		case ScreenPos.Top:
			v = new Vector2(-Screen.width / 2f + rectTransform.position.x, rect.height - localSpacePivot.y + 1);
			break;
		case ScreenPos.Bot:
			v = new Vector2(-Screen.width / 2f + rectTransform.position.x, -rect.height + localSpacePivot.y - 1);
			break;
		case ScreenPos.Left:
			v = new Vector2(-rect.width + localSpacePivot.x - 1, -Screen.height / 2f + rectTransform.position.y);
			break;
		case ScreenPos.Right:
			v = new Vector2(rect.width - localSpacePivot.x + 1, -Screen.height / 2f + rectTransform.position.y);
			break;
		default:
			throw new System.NotImplementedException();
		}
		return v;
	}
	IEnumerator Move_Coroutine(RectTransform uiObject, Vector3 from, Vector3 destination, float duration)
	{
		float timer = duration;
		while (timer >= 0)
		{
			uiObject.transform.position = Vector3.Lerp(from, destination, 1 - timer / duration);
			yield return null;
			timer -= Time.unscaledDeltaTime;
		}
		uiObject.transform.position = destination;
	}
	#endregion Move
	
	#region Scale
	void Scale(RectTransform uiObject, Vector2 startScale, Vector2 finalScale, float duration)
	{
		StartCoroutine(Scale_Coroutine(uiObject, startScale, finalScale, duration));
	}
	
	void Scale(RectTransform uiObject, float startScaleFactor, float finalScaleFactor, float duration)
	{
		StartCoroutine(Scale_Coroutine(uiObject, startScaleFactor * uiObject.localScale, finalScaleFactor * uiObject.localScale, duration));
	}
	
	void Scale(RectTransform uiObject, float startScaleFactor, Vector2 finalScale, float duration)
	{
		StartCoroutine(Scale_Coroutine(uiObject, startScaleFactor * uiObject.localScale, finalScale, duration));
	}
	
	void Scale(RectTransform uiObject, Vector2 startScale, float finalScaleFactor, float duration)
	{
		StartCoroutine(Scale_Coroutine(uiObject, startScale, finalScaleFactor * uiObject.localScale, duration));
	}
	
	void FromScale(RectTransform uiObject, Vector2 startScale, float duration)
	{
		StartCoroutine(Scale_Coroutine(uiObject, startScale, uiObject.transform.localScale, duration));
	}
	
	void FromScale(RectTransform uiObject, float startScaleFactor, float duration)
	{
		StartCoroutine(Scale_Coroutine(uiObject, startScaleFactor * uiObject.transform.localScale, uiObject.transform.localScale, duration));
	}
	
	void Toscale(RectTransform uiObject, Vector2 finalScale, float duration)
	{
		StartCoroutine(Scale_Coroutine(uiObject, uiObject.transform.localScale, finalScale, duration));
	}
	
	void Toscale(RectTransform uiObject, float finalScaleFactor, float duration)
	{
		StartCoroutine(Scale_Coroutine(uiObject, uiObject.transform.localScale, uiObject.transform.localScale * finalScaleFactor, duration));
	}
	IEnumerator Scale_Coroutine(RectTransform uiObject, Vector2 startScale, Vector2 finalScale, float duration)
	{
		float timer = duration;
		while (timer >= 0)
		{
			uiObject.transform.localScale = Vector2.Lerp(startScale, finalScale, 1 - timer / duration);
			yield return null;
			timer -= Time.unscaledDeltaTime;
		}
		uiObject.transform.localScale = finalScale;
	}
	#endregion Scale
	
	#region Color
	
	void FadeOut(Image img, float duration)
	{
		Color endColor = img.color;
		endColor.a = 0;
		StartCoroutine(ColorChange_Coroutine(img, img.color, endColor, duration));
	}
	
	void FadeOut(CanvasGroup group, float duration)
	{
		StartCoroutine(CanvasGroupFade_Coroutine(group, group.alpha, 0, duration));
	}
	
	void FadeOut(Image img, float finalAlpha, float duration)
	{
		Color endColor = img.color;
		endColor.a = finalAlpha;
		StartCoroutine(ColorChange_Coroutine(img, img.color, endColor, duration));
	}
	
	void FadeOut(CanvasGroup group, float finalAlpha, float duration)
	{
		StartCoroutine(CanvasGroupFade_Coroutine(group, group.alpha, finalAlpha, duration));
	}
	
	void FadeIn(Image img, float duration)
	{
		Color endColor = img.color;
		endColor.a = 1;
		StartCoroutine(ColorChange_Coroutine(img, img.color, endColor, duration));
	}
	
	void FadeIn(CanvasGroup group, float duration)
	{
		StartCoroutine(CanvasGroupFade_Coroutine(group, group.alpha, 1, duration));
	}
	
	void FadeIn(Image img, float finalAlpha, float duration)
	{
		Color endColor = img.color;
		endColor.a = finalAlpha;
		StartCoroutine(ColorChange_Coroutine(img, img.color, endColor, duration));
	}
	
	void FadeIn(CanvasGroup group, float finalAlpha, float duration)
	{
		StartCoroutine(CanvasGroupFade_Coroutine(group, group.alpha, finalAlpha, duration));
	}
	
	
	void ChangeColor(Image img, Color startColor, Color endColor, float duration)
	{
		StartCoroutine(ColorChange_Coroutine(img, startColor, endColor, duration));
	}
	
	void FromColor(Image img, Color startColor, float duration)
	{
		StartCoroutine(ColorChange_Coroutine(img, startColor, img.color, duration));
	}
	void ToColor(Image img, Color endColor, float duration)
	{
		StartCoroutine(ColorChange_Coroutine(img, img.color, endColor, duration));
	}
	
	IEnumerator CanvasGroupFade_Coroutine(CanvasGroup group, float startAlpha, float finalAlpha, float duration)
	{
		float timer = duration;
		while (timer >= 0)
		{
			group.alpha = Mathf.Lerp(startAlpha, finalAlpha, 1 - timer / duration);
			yield return null;
			timer -= Time.unscaledDeltaTime;
		}
		group.alpha = finalAlpha;
	}
	
	IEnumerator ColorChange_Coroutine(Image img, Color startColor, Color finalColor, float duration)
	{
		float timer = duration;
		while (timer >= 0)
		{
			img.color = Color.Lerp(startColor, finalColor, 1 - timer / duration);
			yield return null;
			timer -= Time.unscaledDeltaTime;
		}
		img.color = finalColor;
	}
	
	#endregion Color
	
//	void Update () 
//	{
//		RectTransform transform = gameObject.GetComponent<RectTransform>();
//		Image img = gameObject.GetComponent<Image>();
//		CanvasGroup canvasGroup = gameObject.GetComponent<CanvasGroup>();
//		
//		if (Input.GetButtonDown("0"))
//			FadeIn(canvasGroup, 2);
//		if (Input.GetButtonDown("1"))
//			FadeOut(canvasGroup, 0.2f, 2);
//		if (Input.GetButtonDown("2"))
//			ChangeColor(img, Color.red, Color.blue, 2);
//		if (Input.GetButtonDown("3"))
//			FromColor(img, Color.yellow, 2);
//		if (Input.GetButtonDown("4"))
//			ToColor(img, Color.white, 2);
//	}
}
