﻿using UnityEngine;
using System.Collections;

public class Utils
{
	static public IEnumerator WaitForRealTimeCoroutine(float realTime)
	{
		while (realTime > 0)
		{
			realTime -= Time.unscaledDeltaTime;
			yield return null;
		}
	}
}
