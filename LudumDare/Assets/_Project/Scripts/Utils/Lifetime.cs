﻿using UnityEngine;
using System.Collections;

public class Lifetime : MonoBehaviour
{
	public float time;

	void Start ()
	{
		Destroy(gameObject, time);
	}
}
