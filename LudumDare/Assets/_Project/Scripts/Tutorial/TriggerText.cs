﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TriggerText : MonoBehaviour
{
    public enum DiscoveryMode : byte
    {
        None = 0,
        PopIn,
        PopOut,
        FadeIn,
        FadeOut
    }

    public Text worldText = null;
    public float animSpeed = 1f;
    public DiscoveryMode discoveryMode = DiscoveryMode.None;

    [HideInInspector]
    public Animator textAnimator = null;
    
    bool isVisible = false;
    Color savedTextColor;

	public Color gizmosColor;
	public BoxCollider2D collision;

    void Awake()
    {
            worldText.color = new Color(worldText.color.r, worldText.color.g, worldText.color.b, 0f);
        if (worldText == null)
        {
            Debug.LogError("TriggerText: worldText not set properly. Check the reference up!");
            Destroy(this);
        }

        textAnimator = worldText.GetComponent<Animator>();
        if (textAnimator == null)
        {
            Debug.LogError("TriggerText: textAnimator not set properly. Check the reference up!");
            Destroy(this);
        }

        savedTextColor = worldText.color;
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        GameObject obj = col.gameObject.GetComponentInParent<Rigidbody2D>().gameObject;
        if (obj.CompareTag("Player"))
        {
            switch (discoveryMode)
            {
                case DiscoveryMode.PopIn:
                    worldText.color = new Color(savedTextColor.r, savedTextColor.g, savedTextColor.b, 1f);
                    break;
                case DiscoveryMode.PopOut:
                    worldText.color = new Color(savedTextColor.r, savedTextColor.g, savedTextColor.b, 0f);
                    break;
                case DiscoveryMode.FadeIn:
                    textAnimator.speed = animSpeed;
                    textAnimator.SetTrigger("Appear");
                    textAnimator.ResetTrigger("Disappear");
                    break;
                case DiscoveryMode.FadeOut:
                    textAnimator.speed = animSpeed;
                    textAnimator.SetTrigger("Disappear");
                    textAnimator.ResetTrigger("Appear");
                    break;
                default:
                    break;
            }
        }
    }

	void OnDrawGizmos()
	{
		Gizmos.color = gizmosColor;
		Gizmos.DrawCube(transform.position, new Vector3 (collision.size.x, collision.size.y, 1));
	}
}
