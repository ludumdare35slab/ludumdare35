﻿using UnityEngine;
using System.Collections;

public class Teleporter : MonoBehaviour {

	public Teleporter teleporterTarget;
	public ShapeItemObject shapeItemObjectToReact;
	bool playerCameHere = false;

	void OnTriggerEnter2D(Collider2D other)
	{
		if(!playerCameHere)
		{
			ShapeParameters shapeParam = other.gameObject.GetComponentInParent<ShapeParameters>();
			if (shapeParam != null && shapeParam.obj == shapeItemObjectToReact)
			{
				teleporterTarget.TeleportPlayerHere(shapeParam.GetComponentInParent<Player>().transform);
			}
		}
	}

	void OnTriggerExit2D(Collider2D other)
	{
		Player player = other.gameObject.GetComponentInParent<Player>();
		if (player != null)
			playerCameHere = false;
	}

	void OnDrawGizmosSelected()
	{
		if(teleporterTarget != null)
		{
			Gizmos.color = Color.cyan;
			Gizmos.DrawLine(transform.position, teleporterTarget.transform.position);
		}
	}

	public void TeleportPlayerHere(Transform playerToTeleport)
	{
		playerToTeleport.position = transform.position;
		playerCameHere = true;
	}

}
