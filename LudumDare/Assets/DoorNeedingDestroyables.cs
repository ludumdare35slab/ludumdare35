﻿using UnityEngine;
using System.Collections;

public class DoorNeedingDestroyables : MonoBehaviour {

	public DestroyableObjectSpawner[] destroyableObjectsNeeded;
	public GameObject objectsToDeactivate;

	public Color linksToObjectsColor;
	
	public AudioClip clip;

	public bool openImmediately = true;

	public Cinematic cinematicToPlay = null;
	bool jobIsFinished = false;

	// Update is called once per frame
	void Update ()
	{

		if(!jobIsFinished)
		{

			bool isDone = true;

			foreach(DestroyableObjectSpawner curSpawner in destroyableObjectsNeeded)
			{
				if(curSpawner!=null)
				{
					if(curSpawner.obj != null)
						isDone = false;
				}
			}

			if(isDone)
			{
				if (openImmediately)
					Open();
				
				if(cinematicToPlay)
					CinematicManager.Instance.LaunchCinematic(cinematicToPlay);
				jobIsFinished = true;
			}
		}

	}

	public void Open()
	{
		objectsToDeactivate.SetActive(false);
		SoundManager.Instance.PlaySound2D(clip, 0.1f);
	}

	void ActivateAgain()
	{
		objectsToDeactivate.SetActive(true);
		jobIsFinished = false;
	}

	void OnDrawGizmos()
	{
		Gizmos.color = linksToObjectsColor;
		foreach(DestroyableObjectSpawner curSpawner in destroyableObjectsNeeded)
		{
			if(curSpawner != null)
				Gizmos.DrawLine(transform.position, curSpawner.transform.position);

		}
	}

}
